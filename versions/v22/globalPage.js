import uspModifier from "./usp/usp.modifier";
import kpModifier from "./kp/kp-modifier";
import kpV15Modifier from "../v15/kp/kp-modifier";

export default function runGlobalPage() {
  uspModifier.subscribe(() => {
    uspModifier.applyStyles();
    uspModifier.applyChanges();
  });

  kpModifier.subscribe(() => {
    kpModifier.applyChanges(() => {
      kpV15Modifier.applyStyles();
      kpV15Modifier.applyChanges();
    });
  });
}