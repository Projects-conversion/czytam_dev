import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import Usp from "./usp.html";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.addUsp();
  }

  applyStyles() {
    require("./usp.scss");
  }

  applyStylesSG() {
    require("./usp-sg.scss");
  }
  addUsp(selector = "#opis") {
    awaitElements(selector).then(elem => {
      elem.before(Usp);
    });
  }
  addUspListing() {
    awaitElements(".product-list li:nth-child(3)").then(elem => {
      if(jQuery('.jbUspContainer').length == 0){
        elem.before(Usp);
      }
      
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
