/**
 * Its solution to run your local changes in production mode 
 * to apply this you need to do couple of things described in some place 
 * this method simple add your local server file ("magic.js") on your own live-server
 * and use it throw (https://dev.conversion.com:8080) this sub-domain need to be map to
 * https://localhost:8080 in your "/etc/hosts" file (if you use live-server, if not 
 * map this sub-domain to chosen port or ip) 
 */

import { LoadLibraries } from '../../utils/load-libraries';
import libs from '../../lib/libs.dict';

export default function run() {
    if (!window.isConversionProdModeLoaded) {
        LoadLibraries.loadOne(libs.DevConversion).then(() => {
            console.log("Conversion Script Loaded Successfully");
        });
    }
    window.isConversionProdModeLoaded = true; 
}