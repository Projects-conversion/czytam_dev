import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

class DeliveryModifier extends ModifierSubject {
  constructor() {
    super("#main  .alert-box", that => {});
  }

  applyChanges() {
    this.addCompanyCheckbox();
  }

  applyStyles() {
    require("./delivery.scss");
  }

  addCompanyCheckbox() {
    return awaitElements("#main .columns .legend").then(elems => {
      elems.each((i, o) => {
        const legend = jQuery(o);
        if (
          legend
            .find("strong")
            .text()
            .trim() == "lub zamawiającego"
        ) {
          legend.addClass("jbLegend");
        }
      });
    });
  }
}
const deliveryModifier = new DeliveryModifier();
export default deliveryModifier;
