/**
 * 
 * @param {reference to element} elementRef 
 * @param {reference to inserted element*} elementToInsert 
 * @param {function to call on referenced element} functionCall 
 */
export default function callInsertElement(elementRef, elementToInsert, functionCall) {
    switch (functionCall) {
        case 'append':
            elementRef.append(elementToInsert);
            break;
        case 'prepend':
            elementRef.prepend(elementToInsert);
            break;
        case 'after':
            elementRef.after(elementToInsert);
            break;
        case 'before':
            elementRef.before(elementToInsert);
            break;
        default:
            console.log("Method not provided")
            break;
    }
}