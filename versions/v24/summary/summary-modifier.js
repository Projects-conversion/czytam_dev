import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import progressbarObject from "../progressbar/progressbar-modifier";

import summaryDataTemplate from "./summary-data-template.html";
import SSLObject from "../ssl/ssl-modifier";

class SummaryModifier extends ModifierSubject {
  constructor() {
    super("#zgoda", that => {});
  }

  applyChanges() {
    progressbarObject.addProgressBar(
      3,
      ".small-16.large-12.xlarge-13.columns #main",
      false
    );
  }

  applyStyles() {
    require("./summary.scss");
  }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;
