import KpObject from './kp/kp-modifier';


export default function runGlobalPage() {
    KpObject.subscribe(() => {
        KpObject.applyChanges();
        KpObject.applyStyles();
    });
}