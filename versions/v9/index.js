import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";
import runv6 from '../v6';

export default function run(){
    let device = new Device();
    device.setAllowed('mobile');
    runv6();
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v9')){
        console.log("dziala wersja v9");
        window.runningVersionSS = 'v9'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v9');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}