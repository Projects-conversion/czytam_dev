export default function nextWorkingDay(date){//date: after this date is looking for working day 
	const yearNow = date.getFullYear();
	const holidays = [ 
		new Date(yearNow, 0, 1), 
		new Date(yearNow, 0, 6), 
		new Date(yearNow, 4, 1), 
		new Date(yearNow, 4, 3),
		new Date(yearNow, 7, 15),
		new Date(yearNow, 7, 29),
		new Date(yearNow, 10, 1),
		new Date(yearNow, 10, 11),
		new Date(yearNow, 11, 25),
		new Date(yearNow, 11, 26)
	]

	let f = Math.floor,
		G = yearNow % 19,
		C = f(yearNow / 100),
		H = (C - f(C / 4) - f((8 * C + 13)/25) + 19 * G + 15) % 30,
		I = H - f(H/28) * (1 - f(29/(H + 1)) * f((21-G)/11)),
		J = (yearNow + f(yearNow / 4) + I + 2 - C + f(C / 4)) % 7,
		L = I - J,
		monthEaster = 3 + f((L + 40)/44),
		dayEaster = L + 28 - 31 * f(monthEaster / 4);

	const easter = new Date(yearNow, monthEaster-1, dayEaster);
	const easterMonday = new Date(easter.getTime() + 24 * 60 * 60 * 1000);
	const corpusChristi = new Date(easter.getTime() + 24 * 60 * 60 * 1000 *60);
	
	holidays.push(easter);
	holidays.push(easterMonday);
	holidays.push(corpusChristi);

	let nextDay = new Date(date.getTime() + 24 * 60 * 60 * 1000);
	let isNextDayWorking = false;
	while(!isNextDayWorking){
		isNextDayWorking = true;
		if(nextDay.getDay() == 0 || nextDay.getDay()==6){
			isNextDayWorking = false;
			nextDay = new Date(nextDay.getTime() + 24 * 60 * 60 * 1000);
		}
		else{
			for(let i =0; i<holidays.length; i++){
				const isSameYear = nextDay.getFullYear() == holidays[i].getFullYear();
				const isSameMonth = nextDay.getMonth() == holidays[i].getMonth();
				const isSameDay = nextDay.getDate() == holidays[i].getDate();
				if(isSameYear && isSameMonth && isSameDay) isNextDayWorking = false;
			}
			if(!isNextDayWorking) nextDay = new Date(nextDay.getTime() + 24 * 60 * 60 * 1000);
		}
	}

	return nextDay;
}