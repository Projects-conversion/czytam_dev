import kpObject from "./kp/kp-modifier";

export default function runGlobalPage() {
  kpObject.subscribe(() => {
    kpObject.applyStyles();
    kpObject.applyChanges();
  });
}
