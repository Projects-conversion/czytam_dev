import BasketObject from './basket/basket-modifier';
import LoginObject from './login/login-modifier';
import pathChecker from '../../utils/pathChecker';
import WithoutRegister from './without-register/register-modifier';
import WithRegister from './with-register/register-modifier';
import OrdererObject from './orderer/orderer-modifier';
import DeliveryObject from './delivery/delivery-modifier';
import SummaryObject from './summary/summary-modifier';
import TotalSummaryObject from './total-summary/summary-modifier';

export default function runGlobalPage() {
   
    pathChecker(/koszyk.html/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyStyles();
            BasketObject.applyChanges();
        });
    })
       
    pathChecker(/koszyk,zamawiajacy.html/, () => {
        LoginObject.subscribe(() => {
           // LoginObject.applyStyles();
            LoginObject.applyChanges();
        });

        WithoutRegister.subscribe(() => {
           // WithoutRegister.applyStyles();
            WithoutRegister.applyChanges();
        });

       /* pathChecker(/koszyk,zamawiajacy\.html\?req\=true/, () => {
            OrdererObject.subscribe(() => {
                OrdererObject.applyStyles2();
                OrdererObject.applyChanges2();
            });
        });*/

        pathChecker(/koszyk,zamawiajacy\.html$/, () => {
            //window.location.replace("https://czytam.pl/koszyk,dostawa.html");
            //OrdererObject.subscribe(() => {
                //window.location.replace("https://czytam.pl/koszyk,dostawa.html");
              //  OrdererObject.applyStyles();
            //});
            OrdererObject.subscribe(() => {
                OrdererObject.applyStyles2();
                OrdererObject.applyChanges2();
            });
        });
    })

    pathChecker(/rejestracja/, () => {
        WithRegister.subscribe(() => {
            WithRegister.applyStyles();
            WithRegister.applyChanges();
        });
    })

    pathChecker(/koszyk,dostawa|koszyk,przeslij/, () => {
        DeliveryObject.subscribe(() => {
            DeliveryObject.applyStyles();
            DeliveryObject.applyChanges();
        });
    })

    pathChecker(/koszyk,przeslij/, () => {
        SummaryObject.subscribe(() => {
            SummaryObject.applyStyles();
            SummaryObject.applyChanges();
        });

        TotalSummaryObject.subscribe(() => {
            TotalSummaryObject.applyStyles();
            TotalSummaryObject.applyChanges();
        });
    })

    pathChecker(/thankyou.html/, () => {
        TotalSummaryObject.subscribe(() => {
            TotalSummaryObject.applyStyles();
            TotalSummaryObject.applyChanges();
        });
    });
}