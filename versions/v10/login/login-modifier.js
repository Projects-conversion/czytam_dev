import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('#main > form[action="koszyk,zamawiajacy.html"]', (that) => {});
    }
    applyChanges() {
        if(~jQuery('form[action="koszyk,zamawiajacy.html"] label:nth-child(1)').text().indexOf('klientem')){
            this.applyStyles();
        }
        
    }

    applyStyles() {
        require('./login.scss');
    }


}
const loginModifier = new LoginModifier();
export default loginModifier;