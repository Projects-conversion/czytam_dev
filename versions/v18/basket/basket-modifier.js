import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import basketV4Object from "../../v4/basket/basket-modifier";
import basketV5Object from "../../v5/basket/basket-modifier";
import { ajaxSubject } from "../../../utils/ajax.subject";
import guard from "../../../utils/guard";
import { getTextForMobileDelivery } from "../../../utils/delivery-date-getter";
import DeliveryIcon from "../../../assets/sprint1/v1/dostawa.svg";

let showGreenBox = true;
const staticText = [
  "W Twoim koszyku są ostatnie sztuki produktów -<br/> dodanie ich do koszyka nie oznacza rezerwacji.",
  "W Twoim koszyku są produkty promocyjne -<br/> dodanie ich do koszyka nie oznacza rezerwacji.",
  "Dodanie produktów do koszyka nie oznacza rezerwacji. </br> Nie przegap okazji!"
];

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.checkIfProductHasDeliveryNotTomorrow();
    this.applyV4Changes();
    this.applyV5Changes();
    this.addInfoHeader();
    this.addFreeDeliveryInfo();
    this.updateContent();
    this.addFreeDeliveryContainer();

    setTimeout(() => {
      this.changeInfoGratis();
    });
    
  }

  applyV4Changes() {
    this.addFreeDeliveryInfo();
  }

  applyV5Changes() {
    basketV5Object.getProducts();
    basketV5Object.insertTotalSummary();
    basketV5Object.moveAddInfo();
  }

  applyStyles() {
    basketV4Object.applyStyles();
    basketV5Object.applyStyles();
    require("./basket.scss");
  }

  addFreeDeliveryInfo() {
    awaitElements(`#info_gratis`).then(elem => {
      if (showGreenBox) {
        if (jQuery(".jbGreenInfo").length == 0) {
          elem.before(
            `<div class="alert-box jbGreenInfo">
                        <img src="${DeliveryIcon}"/>
                        <span>${getTextForMobileDelivery()}</span></div>`
          );
        }
      }
    });
  }

  checkIfProductHasDeliveryNotTomorrow() {
    awaitElements("form > table > tbody > tr").then(elems => {
      elems.each((i, obj) => {
        const object = jQuery(obj);
        if (
          ~object
            .find(".wysylka")
            .text()
            .indexOf("iedostęp") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("robocze") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("rzedsprzeda")
        ) {
          showGreenBox = false;
          return false;
        } else {
          showGreenBox = true;
        }
      });
    });
  }

  updateContent() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        
        this.checkIfProductHasDeliveryNotTomorrow();
        basketV5Object.getProducts();
        this.changeInfoGratis();
        guard("#main", elem => {
          this.checkIfProductHasDeliveryNotTomorrow();
          basketV5Object.getProducts();
          basketV5Object.insertTotalSummary();
          this.addFreeDeliveryInfo();
          this.changeInfoGratis();
        });
      }
    });
  }

  addInfoHeader() {
    Promise.all([
      awaitElements(".jbHeader"),
      awaitElements(".cart_summary tbody tr")
    ]).then(([elemRef, table]) => {
      table.each((i, obj) => {
        const object = jQuery(obj);

        if (
          ~object
            .find(".cart_description .headline-azure")
            .text()
            .indexOf("sztuki")
        ) {
          this.insertHeader(elemRef, staticText[0]);
          return;
        } else if (
          ~object
            .find(".cart_description .headline-azure")
            .text()
            .indexOf("Promocja")
        ) {
          this.insertHeader(elemRef, staticText[1]);
          return;
        }
      });
      this.insertHeader(elemRef, staticText[2]);
    });
  }

  insertHeader(elemRef, text) {
    if (jQuery(".jbInfoHeader").length == 0) {
      const template = jQuery(`<div class="jbInfoHeader"></div>`);
      template.html(text);
      elemRef.append(template);
    }
  }

  addFreeDeliveryContainer() {
    return awaitElements(".jbTotalSummary").then(elem => {
      if (!jQuery(".jbFreeDeliveryCOntent").length) {
        elem.after(`<div class="jbFreeDeliveryCOntent"></div>`);
      }
    });
  }

  changeInfoGratis() {
    return awaitElements("#info_gratis").then(elem => {
      
      if (~elem.text().indexOf("o bezpłatnej dostawy brakuje")) {
        elem.removeClass("jbHidden");
        const mySubString = elem
          .text()
          .substring(
            elem.text().indexOf("brakuje") + 7,
            elem.text().indexOf("PLN")
          );
        const text = `<span>Do bezpłatnej dostawy brakuje <b>${mySubString}</b> PLN</span>`;
        elem.html(text);
      } else if (~elem.text().indexOf("ostawa jest bezpłatna")) {
        elem.addClass("jbHidden");
        this.addFreeDeliveryContainer().then(() => {
          jQuery(".jbFreeDeliveryCOntent").html(`
        <span class= "title jbDeliveryTitle"> Darmowa dostawa:</span >
        <div class="price jbDeliveryCurrency">
          <span class="value">0.00</span>
          <span class="currency">PLN</span>
        </div>
             `);
        });
      }
    });
  }
}

const basketModifier = new BasketModifier();
export default basketModifier;
