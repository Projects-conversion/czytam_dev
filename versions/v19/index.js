import { LoadingScreen } from "../../utils/loading-screen";
import { Device } from "../../utils/device";
import runGlobalPage from "./globalPage";
import runv11 from "../v11";

export default function run() {
  let device = new Device();
  device.setAllowed("mobile");
  runv11();
  window.runningVersionSSarr = window.runningVersionSSarr || [];
  if (device.isValid() && !~window.runningVersionSSarr.indexOf("v19")) {
    console.log("dziala wersja v19");
    window.runningVersionSS = "v19"; //for page quasii full reloading checking
    window.runningVersionSSarr.push("v19");
    const ls = new LoadingScreen();
    runGlobalPage();
  }
}
