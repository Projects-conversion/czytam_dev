import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import progressbarObject from "../progressbar/progressbar-modifier";
import SSLObject from "../ssl/ssl-modifier";

class OrdererModifier extends ModifierSubject {
  constructor() {
    super('#fdane1, form[action="koszyk,dostawa.html"]', that => {});
  }

  applyChanges() {}

  applyChanges2() {
    progressbarObject.addProgressBar(
      1,
      ".small-16.large-12.xlarge-13.columns #main",
      false
    );
    SSLObject.applyChanges();
  }

  applyStyles() {
    require("./orderer.scss");
  }

  applyStyles2() {
    require("./orderer2.scss");
  }
}
const ordererModifier = new OrdererModifier();
export default ordererModifier;
