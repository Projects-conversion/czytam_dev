import KpObject from "./kp/kp-modifier";
import BasketObject from "./basket/basket-modifier";
import pathChecker from "../../utils/pathChecker";

export default function runGlobalPage() {
  KpObject.subscribe(() => {
    KpObject.applyChanges();
  });
  
  pathChecker(/koszyk/, () => {
    BasketObject.subscribe(() => {
      BasketObject.applyStyles();
      BasketObject.applyChanges();
    });
  });
}
