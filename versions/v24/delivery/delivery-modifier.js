import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { ajaxSubject } from "../../../utils/ajax.subject";
import progressbarObject from "../progressbar/progressbar-modifier";
import SSLObject from "../ssl/ssl-modifier";

class DeliveryModifier extends ModifierSubject {
  constructor() {
    super(".inside-step-shipping", that => {});
  }

  applyChanges() {
    progressbarObject.addProgressBar(
      2,
      ".small-16.large-12.xlarge-13.columns #main",
      false
    );
    SSLObject.applyChanges();

    this.addHeader();
    this.addSortDeliveryInfo();
    this.updateSortDeliveryInfo();
  }

  applyStyles() {
    require("./delivery.scss");
  }

  updateSortDeliveryInfo() {
    ajaxSubject.subscribe(url => {
      if (
        /points|punkty|stats|AuthenticationService|parcelshop|algolia/.test(url)
      ) {
        this.addSortDeliveryInfo();
      }
    });
  }

  addSortDeliveryInfo() {
    return awaitElements(".table-shipping tbody tr td", 3500).then(elems => {
      this.sortDelivery(
        this.jQuery(".table-shipping tbody tr"),
        ".table-shipping tbody"
      );
    });
  }

  addHeader() {
    return awaitElements('form[action="koszyk,przeslij.html"] h3').then(
      elem => {
        elem
          .first()
          .after(`<h3 class="jbTitle">Wybierz rodzaj płatności i dostawy</h3>`);
      }
    );
  }

  sortDelivery(elements, insertTo) {
    elements.sort(function(a, b) {
      let an = jQuery(a).find("td:nth-child(2) label .radio+strong"),
        bn = jQuery(b).find("td:nth-child(2) label .radio+strong");

      an = Number(
        an
          .text()
          .replace(",", ".")
          .replace(/[^0-9\.]+/g, "")
      );
      bn = Number(
        bn
          .text()
          .replace(",", ".")
          .replace(/[^0-9\.]+/g, "")
      );

      if (
        ~jQuery(a)
          .find("td:nth-child(2) label > strong")
          .text()
          .indexOf("Odbiór własny")
      ) {
        an = 100;
      }

      if (an > bn) {
        return 1;
      }
      if (an < bn) {
        return -1;
      }
      return 0;
    });

    elements.detach().appendTo(this.jQuery(insertTo));
  }
}
const deliveryModifier = new DeliveryModifier();
export default deliveryModifier;
