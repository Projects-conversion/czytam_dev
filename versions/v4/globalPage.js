import kpModifier from './kp/kp-modifier';
import BasketObject from './basket/basket-modifier';
import pathChecker from '../../utils/pathChecker';

export default function runGlobalPage() {
    kpModifier.subscribe(() => {
        kpModifier.applyChanges();
        kpModifier.applyStyles();
    });
    pathChecker(/koszyk/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyStyles();
            BasketObject.applyChanges();
        });
    })
}