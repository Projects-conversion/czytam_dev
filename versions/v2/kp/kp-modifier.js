import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { getTextForDelivery } from "../../../utils/delivery-date-getter";
import DeliveryIcon from "../../../assets/sprint1/v1/dostawa.svg";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.addNewDeliveryInfo();
  }

  applyStyles() {
    require("./kp-modifier.scss");
  }

  addNewDeliveryInfo() {
    awaitElements(
      `.productreader > .columns > .row > .columns:not(.col-price)`
    ).then(elem => {
      if (
        ~elem
          .find(".wysylka")
          .text()
          .indexOf("iedostęp") ||
        ~elem
          .find(".wysylka")
          .text()
          .indexOf("robocze") ||
        ~elem
          .find(".wysylka")
          .text()
          .indexOf("rzedsprzeda") ||
        ~elem
          .find(".wysylka")
          .text()
          .indexOf("odruk")
      ) {
      } else {
        jQuery(".wysylka").addClass("jbHide");
        elem.append(`
                <div class="jbDeliveryInforContainer">
                    <div class="jbDeliveryInforContainerIcon"><img src="${DeliveryIcon}"/></div>
                    <div class="jbDeliveryInforContainerText"></div>
                </div>
            `);
        jQuery(".jbDeliveryInforContainerText").html(getTextForDelivery());
      }
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
