import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import SSLTemplate from './ssl-template.html';

class SSLModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges() {
        this.addSSL();
        this.applyStyles();
    }

    applyStyles() {
        require('./ssl.scss');
    }

    addSSL(){
        return awaitElements('.small-9.columns.text-right.show-for-large-up').then(elem => {
            if(this.jQuery('.jbSSL').length == 0){
                elem.after(SSLTemplate);
            }
        });
    }
}
const sslModifier = new SSLModifier();
export default sslModifier;