import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import basketV4Object from "../../v4/basket/basket-modifier";
import basketV5Object from "../../v5/basket/basket-modifier";
import { ajaxSubject } from "../../../utils/ajax.subject";
import guard from "../../../utils/guard";
import { getTextForMobileDelivery } from "../../../utils/delivery-date-getter";
import DeliveryIcon from "../../../assets/sprint1/v1/dostawa.svg";

let showGreenBox = true;

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.checkIfProductHasDeliveryNotTomorrow();
    this.applyV4Changes();
    this.applyV5Changes();

    this.updateContent();
  }

  applyV4Changes() {
    this.addFreeDeliveryInfo();
  }

  applyV5Changes() {
    basketV5Object.changeHeader();
    basketV5Object.getProducts();
    basketV5Object.insertTotalSummary();
    basketV5Object.changeInfoGratis();
    basketV5Object.moveAddInfo();
  }

  applyStyles() {
    basketV4Object.applyStyles();
    basketV5Object.applyStyles();
    require("./basket.scss");
  }

  addFreeDeliveryInfo() {
    awaitElements(`#info_gratis`).then(elem => {
      if (showGreenBox) {
        if (jQuery(".jbGreenInfo").length == 0) {
          elem.before(
            `<div class="alert-box jbGreenInfo">
                        <img src="${DeliveryIcon}"/>
                        <span>${getTextForMobileDelivery()}</span></div>`
          );
        }
      }
    });
  }

  checkIfProductHasDeliveryNotTomorrow() {
    awaitElements("form > table > tbody > tr").then(elems => {
      elems.each((i, obj) => {
        const object = jQuery(obj);
        if (
          ~object
            .find(".wysylka")
            .text()
            .indexOf("iedostęp") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("robocze") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("rzedsprzeda")
        ) {
          showGreenBox = false;
          return false;
        } else {
          showGreenBox = true;
        }
      });
    });
  }

  updateContent() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        this.checkIfProductHasDeliveryNotTomorrow();
        basketV5Object.getProducts();
        guard("#main", elem => {
          this.checkIfProductHasDeliveryNotTomorrow();
          basketV5Object.getProducts();
          basketV5Object.insertTotalSummary();
          basketV5Object.changeInfoGratis();
          this.addFreeDeliveryInfo();
        });
      }
    });
  }
}

const basketModifier = new BasketModifier();
export default basketModifier;
