import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { ajaxSubject } from "../../../utils/ajax.subject";

const freeDelivery = 300;

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    console.log("changes");
    this.addHeader();
    this.checkLoginStatus();
    this.addContainerForDelivery().then(() => {
      this.handleFreeDelivery();
      this.reCountBasket();
    });
  }
  addHeader() {
    awaitElements(".main-section > .row").then(elem => {
      elem.prepend(`<div class="jbHeaderContent">
      Twój koszyk</div>`);
    });
  }

  applyStyles() {
    require("./basket.scss");
  }

  addContainerForDelivery() {
    return awaitElements("#main").then(elem => {
      if (!jQuery(".jbDeliveryContainer").length) {
        elem.append(`<div class="jbDeliveryContainer"></div>`);
      }
    });
  }

  reCountBasket() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        setTimeout(() => {
          this.handleFreeDelivery();
        });
      }
    });
  }

  handleFreeDelivery() {
    awaitElements("#razem > strong").then(elem => {
      const price = parseFloat(elem.text().replace(",", "."));
      this.addContainerForDelivery().then(() => {
        if (price > freeDelivery) {
          jQuery(".jbDeliveryContainer").html(
            "<div>Twoje zamówienie dostarczymy za darmo!</div>"
          );
        } else {
          jQuery(".jbDeliveryContainer").html(
            `<div>Dostawa od 5.99 PLN, bezpłatny odbiór własny w Poznaniu</div> <div>Do darmowej dostawy brakuje ${(
              freeDelivery - price
            )
              .toFixed(2)
              .replace(".", ",")} PLN</div>`
          );
        }
      });

      console.log({ price });
    });
  }

  insertSectionToLogin(isLogged = false) {
    return awaitElements("#shipping-costs").then(elem => {
      let template = "";
      if (!isLogged) {
        template = ` <div class="jbNoLogin">
                    <span>Posiadasz konto na Czytam.pl?</span>
                    <span><a href="https://czytam.pl/konto.html">Zaloguj się</a> aby wykorzystać rabat dla klientów</span>
                </div>`;
      } else {
        template = jQuery(".logout");
      }
      elem.before(template);
    });
  }

  checkLoginStatus() {
    return awaitElements("#header-account .link-registered").then(elem => {
      if (~elem.text().indexOf("Wyloguj")) {
        this.insertSectionToLogin(true);
      } else {
        this.insertSectionToLogin();
      }
    });
  }
}

const basketModifier = new BasketModifier();
export default basketModifier;
