import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import DeliveryIcon from '../../../assets/sprint2/v5/truck_small.svg';

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => { });
  }

  applyChanges(callback = () => { }) {
    awaitElements(".tab-gallery > dl > dd").then(elem => {
      if (elem.length < 4) {
        this.changeButtonText();
        this.addDelivery();
        this.addRedirectToBasket();
        this.applyStyles();
        callback();
      }
    });
  }

  applyStyles() {
    require("./kp.scss");
  }

  changeButtonText() {
    awaitElements('.add-cart-big span').then(elem => {
      elem.text('Dodaj do koszyka');
    });
  }

  addRedirectToBasket() {
    awaitElements('.add-cart-big').then(elem => {
      elem.click(() => {
        if (jQuery('.jbRedirectToBasket').length == 0) {
          elem.after(`<a href="https://czytam.pl/koszyk.html"><span class="jbRedirectToBasket">Przejdź do koszyka</span></a>`);
          elem.addClass('jbAddButton');
          jQuery('#schowek').addClass('jbHide');
        }
      })
    });
  }

  addDelivery() {
    awaitElements('.wysylka').then(elem => {
      elem.addClass('jbHide');
      const selector = (jQuery('.add-cart-big').length > 0) ? jQuery('.add-cart-big') : jQuery('#schowek').children();
      selector.parent().before(`<div class="jbWysylka">
            <img src="${DeliveryIcon}"/>
            <span>Wysyłka: ${elem.find('strong').text()}</span>
          </div>`);
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
