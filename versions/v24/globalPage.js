import BasketObject from "./basket/basket-modifier";
import LoginObject from "./login/login-modifier";
import pathChecker from "../../utils/pathChecker";
import WithoutRegister from "./without-register/register-modifier";
import WithRegister from "./with-register/register-modifier";
import OrdererObject from "./orderer/orderer-modifier";
import DeliveryObject from "./delivery/delivery-modifier";
import SummaryObject from "./summary/summary-modifier";
import TotalSummaryObject from "./total-summary/summary-modifier";

export default function runGlobalPage() {
  pathChecker(/koszyk.html/, () => {
    BasketObject.subscribe(() => {
      BasketObject.applyStyles();
      BasketObject.applyChanges();
    });
  });

  pathChecker(/koszyk,zamawiajacy.html/, () => {
    LoginObject.subscribe(() => {
      LoginObject.applyChanges();
    });

    WithoutRegister.subscribe(() => {
      WithoutRegister.applyChanges();
    });

    pathChecker(/koszyk,zamawiajacy\.html$/, () => {
      OrdererObject.subscribe(() => {
        OrdererObject.applyStyles2();
        OrdererObject.applyChanges2();
      });
    });
  });

  pathChecker(/rejestracja/, () => {
    WithRegister.subscribe(() => {
      WithRegister.applyStyles();
      WithRegister.applyChanges();
    });
  });

  pathChecker(/koszyk,dostawa/, () => {
    DeliveryObject.subscribe(() => {
      DeliveryObject.applyStyles();
      DeliveryObject.applyChanges();
    });
  });

  pathChecker(/koszyk,przeslij/, () => {
    SummaryObject.subscribe(() => {
      SummaryObject.applyStyles();
      SummaryObject.applyChanges();
    });

    DeliveryObject.subscribe(() => {
      DeliveryObject.applyStyles();
      DeliveryObject.applyChanges();
    });
  });

  pathChecker(/thankyou.html/, () => {
    TotalSummaryObject.subscribe(() => {
      TotalSummaryObject.applyStyles();
      TotalSummaryObject.applyChanges();
    });
  });
}
