import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class KpModifier extends ModifierSubject {
    constructor() {
        super('.row.productreader', (that) => {});
    }

    applyChanges(){
       this.addDeliveryDateInfo();
       this.addNewPriceContent();
       this.changeGallery();
       this.goToBasket();
       this.changePromoInfo();
       this.changeButton();
    }

    applyStyles() {
        require('./kp.scss');
    }

    addDeliveryDateInfo(){
        return awaitElements('.wysylka').then(elem => {
            elem.addClass('jbHide');
            elem.after(`<div class="jbWysylka wysylka">
                <span>Wysyłka:</span>
                <strong>${elem.find('strong').text()}</strong>
            </div>`)
        });
    }

    changeButton(){
        return awaitElements('.add-cart-big').then(elem => {
            if(~elem.find('span').text().indexOf('W koszyku')){
                elem.addClass('jbInBasket');
            }else{
                elem.addClass('jbToBasket');
            }
        });
    }

    goToBasket(){
        return awaitElements('.add-cart-big').then(elem => {
            elem.click(() => {
                jQuery('.add-cart-big').removeClass('jbToBasket');
                if(jQuery('.jbGoToBasket').length == 0) {
                    jQuery('.add-clipboard').parent().before(`
                    <div class="jbGoToBasket">
                        <a href="https://czytam.pl/koszyk.html">Przejdź do koszyka  ></a>
                    </div>
                    `);
                    jQuery('.add-clipboard').addClass('jbHide');
                }
            });
        });
    }

    changeGallery(){
        return awaitElements('.tab-gallery').then(elem => {
            if(elem.find('dd').length == 1) {
                elem.addClass('jbNewGallery');
            }
        });
    }

    changePromoInfo(){
        return awaitElements('.row .small-16.medium-8.columns').then(elem => {
            elem.contents()
            .filter(function() {
                var val = this.nodeValue;
                if(val != null){
                    if(~val.indexOf('Promocja')){
                        this.nodeValue = 'Promocja:';
                        return true;
                    }
                    else if(~val.indexOf('Best')){
                        return true;
                    }   
                }
            }).wrap( "<span class=jbPromo></span>" ).end();

            if(this.jQuery('.jbPromo').length > 0){
                const template = `<div class="jbPromoDiv">${jQuery('.jbPromo').prop('outerHTML')}${this.jQuery('.small-16.medium-8.columns .headline-azure').prop('outerHTML')}</div>`
                jQuery('.small-16.columns.col-price.show-for-small-only').append(template);
            }
        });
    }

    addNewPriceContent(){
        return awaitElements('.jbWysylka').then(elem => {
            const template = jQuery(`
            <div class="jbTotalPrice">
                <div class="jbOldPrice">
                    <span class="title">Cena:</span>
                    <span class="value"></span>
                </div>
                <div class="jbNewPrice">
                    <span class="title">Nasza cena:</span>
                    <span class="value"></span>
                </div>
                <span class="jbDiscount"></span>
            </div>`);
            const oldPrice = jQuery('.small-16.columns.col-price.show-for-small-only .oldPirce strong');
            const newPrice = jQuery('.small-16.columns.col-price.show-for-small-only .price strong');
            const discount = jQuery('.small-16.columns.col-price.show-for-small-only .save');

            if(jQuery('#powiadom_info').length > 0) { 
                
                template.find('.jbOldPrice .value').text(`${oldPrice.text()}`);
                template.find('.jbNewPrice .value').text(`${newPrice.text()}`);
                template.find('.jbDiscount').text(discount.text());
                elem.after(template);

                jQuery('.jbTotalPrice').attr('style', 'margin-top: 50px !important');
            }
            else if(oldPrice.length == 0){
                template.find('.jbOldPrice').addClass('jbHide');
                template.find('.jbDiscount').addClass('jbHide');
                elem.after(template);
            }else{
                template.find('.jbOldPrice .value').text(`${oldPrice.text()}`);
                template.find('.jbNewPrice .value').text(`${newPrice.text()}`);
                template.find('.jbDiscount').text(discount.text());
                elem.after(template);
            }
        });
    }

}
const kpModifier = new KpModifier();
export default kpModifier;