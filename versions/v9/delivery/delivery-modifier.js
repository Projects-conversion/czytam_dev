import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import ProgressBarObject from '../progressbar/progressbar-modifier';
import SSLHeader from '../ssl-header/ssl-header-modifier';

class DeliveryModifier extends ModifierSubject {
    constructor() {
        super('.inside-step-shipping', (that) => {});
    }

    applyChanges(){
        SSLHeader.applyChanges();
        ProgressBarObject.applyChanges(2, '#order_step');
    }

    applyStyles(){
        require('./delivery.scss');
    }

}
const deliveryModifier = new DeliveryModifier();
export default deliveryModifier;