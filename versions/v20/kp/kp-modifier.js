import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import Usp from "./usp.html";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.addUsp();
  }

  applyStyles() {
    require("./kp.scss");
  }
  addUsp() {
    awaitElements("#opis").then(elem => {
      elem.before(Usp);
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
