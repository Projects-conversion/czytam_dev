import kpModifier from "./kp/kp-modifier";
import kpV15Modifier from "../v15/kp/kp-modifier";

export default function runGlobalPage() {
  kpModifier.subscribe(() => {
    kpModifier.applyChanges(() => {
      kpV15Modifier.applyStyles();
      kpV15Modifier.applyChanges();
    });
  });
}
