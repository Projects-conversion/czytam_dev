import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { ajaxSubject } from "../../../utils/ajax.subject";

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.reCountBasket();
    this.watchLoaderData(dL => {
      this.addPromoInfo(dL);
    });
  }

  applyStyles() {
    require("./basket-modifier.scss");
  }

  watchLoaderData(callback = () => {}) {
    if (!window.dataLayer[3]) {
      setTimeout(() => {
        this.watchLoaderData(callback);
      }, 10);
    } else {
      callback(window.dataLayer[3]);
    }
  }

  reCountBasket() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        setTimeout(() => {
          this.watchLoaderData(dL => {
            this.addPromoInfo(dL);
          });
        });
      }
    });
  }

  addPromoInfo({ products }) {
    awaitElements(".cart_summary tr .cart_description > div > a").then(
      elems => {
        products.forEach(singleProduct => {
          elems.each((i, o) => {
            const productLink = jQuery(o);
            const trProduct = jQuery(o)
              .parent()
              .parent()
              .parent();
            if (~productLink.attr("href").indexOf(singleProduct.id)) {
              const [unit, summary] = trProduct.find("> .cart_unit").toArray();
              const unitJ = jQuery(unit);
              const summaryJ = jQuery(summary);
              const discountPercent = parseInt(
                (1 -
                  Number(singleProduct.promoPrice) /
                    Number(singleProduct.regularPrice)) *
                  100
              );
              unitJ.append(
                `
              <div class="jbPrice">
                <del>${Number(singleProduct.regularPrice).toFixed(2)} PLN</del>
                <div class="jbValue">
                <span class="cart_unit">${Number(singleProduct.promoPrice)
                  .toFixed(2)
                  .replace(".", ",")}</span><span class="currency">PLN</span></div>
                <span class="jbPercent">Oszczędzasz ${discountPercent} %</span>
              </div>
              `
              );
              summaryJ.append(
                `
              <div class="jbPrice">
                <del>${Number(
                  trProduct.find(".cart_quantity input").val() *
                    singleProduct.regularPrice
                ).toFixed(2)} PLN</del>
                <div class="jbValue">
                <span class="cart_unit">${Number(
                  trProduct.find(".cart_quantity input").val() *
                    singleProduct.promoPrice
                )
                  .toFixed(2)
                  .replace(".", ",")}</span><span class="currency">PLN</span></div>
                <span class="jbPercent">Oszczędzasz ${discountPercent} %</span></div>
              </div>
              `
              );
            }
          });
        });
      }
    );
  }
}
const basketModifier = new BasketModifier();
export default basketModifier;
