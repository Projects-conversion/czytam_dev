import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import UspTemplate from './usp-template.html';

class KpModifier extends ModifierSubject {
    constructor() {
        super('.productreader', (that) => {});
    }

    applyChanges(){
        this.addNewUsp();
    }

    applyStyles() {
        require('./kp-modifier.scss');
    }
    
    addNewUsp(){
        awaitElements('.productreader').then((productDetails)=>{
            var date = new Date();
            const template = jQuery(UspTemplate);

            if(date.getHours() >= 15){
                template.find('#jbUspDynamicText').html(`Ciesz się wymarzoną pozycją <br>w błyskawicznym tempie<br> - wysyłka w
                24h!`);
            }

            productDetails.before(template)
        });
    }

}
const kpModifier = new KpModifier();
export default kpModifier;