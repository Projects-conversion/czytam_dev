import LoginObject from '../v9/login/login-modifier';
import RegisterObject from './register/register-modifier';
import DeliveryObject from './delivery/delivery-modifier';
import SummaryObject from './summary/summary-modifier';
import TotalSummaryObject from '../v9/total-summary/summary-modifier';
import RedirectObject from './redirect/redirect-modifier';
import pathChecker from '../../utils/pathChecker';
import WithRegister from './with-register/register-modifier';

export default function runGlobalPage() {

    pathChecker(/koszyk,zamawiajacy\.html$/, () => {
        RegisterObject.subscribe(() => {
            RegisterObject.applyChangesV10();
        });

        LoginObject.subscribe(() => {
            LoginObject.applyChanges();
            LoginObject.applyStyles();
        });

        RedirectObject.subscribe(() => {
            RedirectObject.applyChanges();
           // RedirectObject.applyStyles();
        });
    });

    pathChecker(/rejestracja/, () => {
        RegisterObject.subscribe(() => {
            RegisterObject.applyStyles();
            RegisterObject.applyChangesV9();
            RegisterObject.applyChangesV10();
        });

        RedirectObject.subscribe(() => {
            RedirectObject.applyChanges();
        });

        WithRegister.subscribe(() => {
            WithRegister.applyStyles();
          });
    });

    pathChecker(/koszyk,dostawa|koszyk,przeslij/, () => {
        DeliveryObject.subscribe(() => {
            DeliveryObject.applyStyles();
            DeliveryObject.applyChanges();
        });
    });

    pathChecker(/koszyk,przeslij/, () => {
        SummaryObject.subscribe(() => {
            SummaryObject.applyStyles();
            SummaryObject.applyChanges();
        });
    })

    pathChecker(/thankyou.html/, () => {
        TotalSummaryObject.subscribe(() => {
            TotalSummaryObject.applyStyles();
            TotalSummaryObject.applyChanges();
        });
    });
}