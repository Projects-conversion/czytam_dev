import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class ListingModifier extends ModifierSubject {
    constructor() {
        super('.headline.icon-katalog', (that) => {});
    }

    applyChanges(){
        this.redirectToKp();
    }

    redirectToKp(){
        awaitElements('.product-list li').then(elem => {
            elem.each((i, obj) => {
                
                const object = jQuery(obj);
                object.find('img').removeAttr('data-tooltip').removeAttr('data-options').removeAttr('data-selector');
            });
        })
    }
}
const listingModifier = new ListingModifier();
export default listingModifier;