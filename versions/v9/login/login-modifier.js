import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBarObject from '../progressbar/progressbar-modifier';
import SSLHeader from '../ssl-header/ssl-header-modifier';

class LoginModifier extends ModifierSubject {
    constructor() {
        super('#main > form[action="koszyk,zamawiajacy.html"]', (that) => {});
    }
    applyChanges(){
        SSLHeader.applyChanges();
        ProgressBarObject.applyChanges(1, '#order_step');
    }

    applyStyles(){
        require('./login.scss');
    }
    
}
const loginModifierObject = new LoginModifier();
export default loginModifierObject;