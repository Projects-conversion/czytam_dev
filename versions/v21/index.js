import { LoadingScreen } from "../../utils/loading-screen";
import { Device } from "../../utils/device";
import runGlobalPage from "./globalPage";
import runv11 from "../v11";

export default function run() {
  let device = new Device();
  device.setAllowed("mobile");
  runv11();
  window.runningVersionSSarr = window.runningVersionSSarr || [];
  if (device.isValid() && !~window.runningVersionSSarr.indexOf("v21")) {
    console.log("dziala wersja v21");
    window.runningVersionSS = "v21"; //for page quasii full reloading checking
    window.runningVersionSSarr.push("v21");
    const ls = new LoadingScreen();
    runGlobalPage();
  }
}
