import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class SummaryModifier extends ModifierSubject {
    constructor() {
        super('#zgoda', (that) => {});
    }

    applyChanges(){
       this.changeDeliveryText();
       this.expandAgreement();
       this.moveContract();
       this.movePolicy();
    }

    applyStyles(){
        require('./summary.scss');
    }

    changeDeliveryText(){
        awaitElements('.cart-total_shipping').then(elem => {
            elem
            .contents()
            .filter(function() {
              return this.nodeType == 3; //Node.TEXT_NODE
            })
            .remove();

            elem.find('.right.hide-small').html(`Koszt<br>dostawy`);
        });
    }

    expandAgreement(){
        return awaitElements('a[target="polityka"]').then(elem => {
            const template = jQuery(`<label><span class="jbAgree">Administratorem danych osobowych jest P.H.U. Tami Wojciech Sterna... &nbsp; &nbsp;Rozwiń</span><i class="jbArrow arrow down"/></label>`);

            template.find('.jbAgree').click(() => {
                jQuery('.jbAgree').html(`<span class="jbAgree">Administratorem danych osobowych jest P.H.U. Tami Wojciech Sterna z siedzibą przy ul. Piotrowskiej 12, 61-353 Poznań, NIP: 7821006266. Dane będą przetwarzane w celu świadczenia usług drogą elektroniczną zgodnie z Regulaminem oraz realizacji zamówień złożonych na czytam.pl. Podanie danych jest dobrowolne, jednak konieczne do obsługi i realizacji zamówienia oraz rejestracji zamówienia w serwisie czytam.pl (opcjonalne). Masz prawo dostępu do treści swoich danych, ich poprawienia czy cofnięcia udzielonych zgód na przetwarzanie danych w każdym czasie. Więcej informacji o przetwarzaniu danych osobowych w naszej <a href="/polityka-prywatnosci.html" target="polityka"><u>Polityce Prywatności</u></a></span>`);
            });
            elem.parent().html(template);
        });
    }

    moveContract(){
        return Promise.all([
            awaitElements('.small-16.small-full.columns'),
            awaitElements('.off-canvas-wrap section .text-right .long-check'),
        ]).then(([elemRef, elemTo]) => {
            const cpyElem = elemRef.clone(true, true).addClass('jbContract');
            elemTo.after(cpyElem);
            elemRef.addClass('jbHide');
        });
    }

    movePolicy(){
        return Promise.all([
            awaitElements('#main > label'),
            awaitElements('.small-16.small-full.columns:not(.jbContract)')
        ]).then(([elemRef, elemTo]) => {
            const cpyElem = elemRef.clone(true, true).addClass('jbContract');
            elemTo.after(cpyElem);
            elemRef.addClass('jbHide');
        });
    }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;