import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import promoInfo from '../../v14/kp/promo-info.html';

class KpModifier extends ModifierSubject {
    constructor() {
        super(".productreader", that => { });
    }

    applyChanges() {
        this.movePromoBadge();
        this.changePromoPriceText();
        this.changeDeliveryTextColor();
    }

    applyStyles() {
        require("./kp.scss");
    }

    changePromoPriceText() {
        Promise.all([
            awaitElements('.rectangle-promo'),
            awaitElements('.col-price .oldPirce'),
            awaitElements('.col-price .price')
        ]).then(([orygBadge, oldPrice, newPrice]) => {
            if (~orygBadge.find('span').text().indexOf('Promo')) {
                oldPrice[0].childNodes[0].nodeValue = 'Cena przed promocją';
                newPrice[0].childNodes[0].nodeValue = 'Aktualna cena';
            } else {
                if(~orygBadge.find('span').text().indexOf('Książka')){
                    oldPrice[0].childNodes[0].nodeValue = 'Sugerowana cena producenta';
                }
                else {
                    oldPrice[0].childNodes[0].nodeValue = 'Sugerowana cena detaliczna';
                }
            }
        })
    }

    changeDeliveryTextColor() {
        awaitElements('.row > .small-16.medium-8.columns > .wysylka > strong').then(elem => {
            if (elem.css('color') == 'rgb(242, 87, 73)') {
                elem.addClass('jbBlackColor');
            }
        });
    }

    movePromoBadge() {
        Promise.all([
            awaitElements('.rectangle-promo.orange'),
            awaitElements('.col-price .oldPirce'),
        ]).then(([orygBadge, newBadge]) => {
            newBadge.before(jQuery(orygBadge[0]).clone(true, true).addClass('jbNewBadge'));
            orygBadge.addClass('jbHide');
            this.addPromoInfo();
        });
    }

    addPromoInfo() {
        awaitElements(".save").then(elem => {
            if (~jQuery('.rectangle-promo span').text().indexOf('Promo')) {
                elem.addClass('jbSave');
                elem.after(promoInfo);
            }
        });
    }
}
const kpModifier = new KpModifier();
export default kpModifier;
