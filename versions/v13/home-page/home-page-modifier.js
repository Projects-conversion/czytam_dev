import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import UspTemplate from './usp-template.html';

class HomePageModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges(){
        this.addUsp();
    }

    applyStyles(){
        require('./home-page.scss');
    }

    addUsp(){
        awaitElements('#header-slider').then(elem => {
            elem.prepend(UspTemplate);
        });
    }
}
const homePageModifier = new HomePageModifier();
export default homePageModifier;