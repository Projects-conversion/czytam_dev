/**
 * Elements callback
 *
 * @callback elementsCallback
 * @param {string} error - Populated if any guard function returned an error
 * @param {HTMLElement[]} elements - Array of found elements
 */

/**
 * Await guard
 *
 * @callback awaitGuard
 * @returns {String|null}
 *
 */

import guard from "./guard";

/**
 * Wait until element is found or is stopped by guard
 *
 * @param {string} query - CSS selectors query to find
 * @param {elementsCallback} callback - called if elements have been found
 * @param {awaitGuard[]} guards - conditionals to stop await function
 * @param {Number} timeout=1 - await iteration timeout
 */
export function awaitElements(query, callback, guards, timeout) {
    if (typeof callback == 'function') {
        awaitElementsCallback(query, callback, guards, timeout);
    } else {
        return awaitElementsPromise(query, timeout);
    }
}

/**
 * Vanilla JS elements selector on callbacks.
 * @param {string} query
 * @param {elementsCallback} callback
 * @param {awaitGuard[]} guards
 * @param {Number} timeout
 */
export function awaitElementsCallback(query, callback, guards = [], timeout = 10) {
    const elements = document.querySelectorAll(query);
    if (elements.length > 0) {
        callback(null, elements);
    } else {
        const errors = guards.map(guard => guard()).filter(error => error);
        if (errors.length === 0) {
            setTimeout(() => { awaitElementsCallback(query, callback, guards, timeout); }, timeout);
        } else {
            callback(errors[0]);
        }
    }
}

/**
 * Vanilla JS elements selector using ES6 Promises
 * @param {string} query
 * @param {number} timeout
 * @returns {Promise}
 */
export function awaitElementsPromise(query, timeout = 3000) {
    return new Promise((resolve, reject) => {
        awaitElements(query, (err, elems) => {
            if (elems) {
                resolve(jQuery(elems));
            } else {
                reject(false);
            }
        }, [createTimeoutGuard(timeout)]);
    });
}

/**
 * Guard that checks if document is ready
 *
 * @type awaitGuard
 */
export function isDOMReadyGuard() {
    return document.readyState === 'complete' ? 'Document is ready' : null;
}


/**
 * Creates guard that return error after given time
 * @param {Numer} timeout
 */
export function createTimeoutGuard(timeout) {
    let shouldStop = false;
    setTimeout(() => shouldStop = true, timeout);
    return () => shouldStop;
}

export function isDOMReady() {
    return document.readyState === 'complete' ? 'Document is ready' : null;
}


export function createElement(template) {
    const wrapper = document.createElement('DIV');
    wrapper.innerHTML = template;
    return wrapper.children[0];
}