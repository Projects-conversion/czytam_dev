import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import BasketV14 from "../../v14/basket/basket-modifier";
import infoHeader from "./info-header.html";
import { ajaxSubject } from "../../../utils/ajax.subject";

const staticText = [
  "W Twoim koszyku są ostatnie sztuki produktów - dodanie ich do koszyka nie oznacza rezerwacji. Nie przegap okazji!",
  "W Twoim koszyku są produkty promocyjne - dodanie ich do koszyka nie oznacza rezerwacji. Nie przegap okazji!",
  "Dodanie produktów do koszyka nie oznacza rezerwacji. Nie przegap okazji!"
];

class KpModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.applyChangesBasketV14();
    this.addInfoHeader();
    this.insertFreeDelivery();
  }

  applyStyles() {
    require("./basket.scss");
  }

  applyChangesBasketV14() {
    this.reCountBasket();
    this.watchLoaderData(dL => {
      BasketV14.addPromoInfo(dL);
    });
    BasketV14.applyStyles();
  }

  watchLoaderData(callback = () => {}) {
    if (!window.dataLayer[3]) {
      setTimeout(() => {
        this.watchLoaderData(callback);
      }, 10);
    } else {
      callback(window.dataLayer[3]);
    }
  }

  reCountBasket() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        
        setTimeout(() => {
          this.watchLoaderData(dL => {
            BasketV14.addPromoInfo(dL);
            this.insertFreeDelivery();
            this.addInfoHeader();
          });
        });
      }
    });
  }

  copyTR() {
    awaitElements("#main > form > table > tfoot > tr:nth-child(1)").then(
      elem => {
        elem.after(elem.clone(true, true));
      }
    );
  }

  addInfoHeader() {
    Promise.all([
      awaitElements(".headline-border-bottom.headline"),
      awaitElements(".cart_summary tbody tr")
    ]).then(([elemRef, table]) => {
      let addedHeader = false;
      table.each((i, obj) => {
        const object = jQuery(obj);
        if (
          ~object
            .find(".cart_description .headline-azure")
            .text()
            .indexOf("sztuki")
        ) {
          this.insertHeader(elemRef, staticText[0]);
          addedHeader = true;
          return false;
        } else if (
          ~object
            .find(".cart_description .headline-azure")
            .text()
            .indexOf("Promocja")
        ) {
          this.insertHeader(elemRef, staticText[1]);
          addedHeader = true;
          return false;
        }
      });

      if(!addedHeader){
        this.insertHeader(elemRef, staticText[2]);
        return false;
      }
    });
  }

  insertFreeDelivery() {
    awaitElements(".cart_total_price").then(elem => {
      let price = parseFloat(elem.text().replace(",", "."));
      if (price >= 299) {
        if (jQuery("#info_gratis").hasClass("jbHide") == false) {
          jQuery("#info_gratis").addClass("jbHide");
          jQuery(
            "#main > form > table > tfoot > tr:nth-child(1) > td:nth-child(4)"
          )
            .attr("colspan", "2")
            .addClass("cart_total_quantity jb")
            .prepend(
              `<div class="jbSummary"><span>Razem</span><span>Darmowa dostawa</span></div>`
            );
          elem.html(
            `<div class="jbTotal"><strong>${elem
              .find("strong")
              .text()}</strong>PLN</div>`
          );
          jQuery(".cart_total_price").append(
            `<div class="jbFreeDelivery"><span>0,00</span>PLN</div>`
          );
        }
      } else {
        jQuery(
          "#main > form > table > tfoot > tr:nth-child(1) > td:nth-child(4)"
        )
          .attr("colspan", "2")
          .addClass("cart_total_quantity jb")
          .prepend(`<div class="jbSummary2"><span>Razem</span></div>`);
        elem.html(
          `<div class="jbTotal"><strong>${elem
            .find("strong")
            .text()}</strong>PLN</div>`
        );
        jQuery(".cart_delete_link").addClass("jbNewDeleteLink");
        if (jQuery("#info_gratis").hasClass("jbHide") == true) {
          jQuery("#info_gratis").removeClass("jbHide");
        }
      }
    });
  }

  insertHeader(elemRef, text) {
    if (jQuery(".jbInfoHeader").length == 0) {
      const template = jQuery(infoHeader);
      template.find("span").text(text);
      elemRef.append(template);
    }else{
      jQuery('.jbInfoHeader span').text(text);
    }
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
