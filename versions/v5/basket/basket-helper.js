import { awaitElements } from "../../../utils/elements";
import CloseIcon from "../../../assets/sprint1/v1/close.svg";

export function createItemTemplate(object) {
  let inputElem = object
    .find(".cart_quantity input")
    .clone(true, true)
    .addClass("jbProductCount")
    .prop("outerHTML");
  inputElem =
    typeof inputElem === "undefined"
      ? `<span class="jbCounter">${object.find(".cart_quantity").text()}</span>`
      : `<input type="button" value="-" class="jbProductItemContentButtonsMinus"/>
    ${inputElem}
    <input type="button" value="+" class="jbProductItemContentButtonsAdd"/>`;

  const prices = object
    .find(".cart_unit:last-child span")
    .text()
    .split("PLN");
  let promoElem =
    prices.length > 2
      ? `<span class="jbProductItemContentPromoPrice">${
          prices[0]
        } PLN</span><span class="jbProductItemContentPrice">${
          prices[1]
        } PLN</span>`
      : `<span class="jbProductItemContentPrice">${object
          .find(".cart_unit:last-child span")
          .text()}</span>`;

  const result = `<div class="jbProductItem">
            <a href="${object.find(".cart_image a").attr("href")}">
            <img class="jbProductItemImg" src="${object
              .find(".cart_image img")
              .attr("src")}"/></a>
            <div class="jbProductItemContent">
                <a href="${object.find(".cart_description a").attr("href")}">
                <span class="jbProductItemContentTitle">${object
                  .find(".cart_description a")
                  .text()}</span></a>
                <div class="jbProductItemContentButtons">        
                    ${inputElem}
                </div>
                ${promoElem}
            </div>
            <img class="jbProductItemRemove" src="${CloseIcon}"/>
        </div>`;

  return result;
}
///kosz_update.php?u=15586592 Request URL: https://czytam.pl/include/kosz_update.php?u=15586672

export function addClickHandlers(template) {
  template.find(".jbProductItemContentButtonsMinus").unbind();
  template.find(".jbProductItemContentButtonsMinus").click(e => {
    const currObject = jQuery(e.currentTarget).parent();

    const actVal = currObject.find(".jbProductCount").val();
    const newVal = Number(actVal) - 1;

    if (newVal >= 0) {
      currObject.find(".jbProductCount").val(newVal);
      currObject.find(".jbProductCount").keyup();
    }
  });

  template.find(".jbProductItemContentButtonsAdd").unbind();
  template.find(".jbProductItemContentButtonsAdd").click(e => {
    const currObject = jQuery(e.currentTarget).parent();

    const actVal = currObject.find(".jbProductCount").val();
    const newVal = Number(actVal) + 1;

    //if(newVal <= 9){
    currObject.find(".jbProductCount").val(newVal);
    currObject.find(".jbProductCount").keyup();
    //}
  });

  template.find(".jbProductItemRemove").unbind();
  template.find(".jbProductItemRemove").click(e => {
    const actId = jQuery(e.currentTarget)
      .parent()
      .find(".jbProductCount")
      .attr("id")
      .replace("ile_", "");
    jQuery(`input[value=${actId}]`).click();
    jQuery(".button-delate.show-small").click();

    dataLayer[3].products = dataLayer[3].products.filter(
      ({ id }) =>
        !~jQuery(e.currentTarget)
          .parent()
          .find("> a")
          .attr("href")
          .indexOf(id)
    );
  });
}
