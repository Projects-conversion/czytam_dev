const path = require("path");
("use strict");

module.exports = {
  resolve: {
    alias: {
      ss: __dirname, // for js modules
      "./ss": __dirname // for modules referenced in less and html files
    }
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.html/,
        loader: "html-loader"
      },
      {
        test: /\.png/,
        loader: "url-loader"
      },
      {
        test: /\.svg/,
        loader: "url-loader"
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader"
      },
      {
        test: /\.scss$/,
        loader: "style-loader!css-loader!sass-loader"
      }
    ]
  }
};
