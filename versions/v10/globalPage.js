import DeliveryObject from "./delivery/delivery-modifier";
import DeliveryObjectSummary from "./delivery-summary/delivery-modifier";
import paymentModifierObject from "./payment/payment";
import LoginObject from './login/login-modifier';
import WithRegister from './register/register-modifier';

import checkPath from "../../utils/pathChecker";

export default function runGlobalPage() {
  checkPath(/koszyk,zamawiajacy\.html$/, () => {
    LoginObject.subscribe(() => {
      // LoginObject.applyStyles();
       LoginObject.applyChanges();
   });
    
      DeliveryObject.subscribe(() => {
        DeliveryObject.applyChanges();
        DeliveryObject.applyStyles();
      });
    DeliveryObjectSummary.subscribe(() => {
      DeliveryObjectSummary.applyChanges();
      DeliveryObjectSummary.applyStyles();
    });
  });

  checkPath(/rejestracja/, () => {
    WithRegister.subscribe(() => {
        WithRegister.applyStyles();
        WithRegister.applyChanges();
    });
})

  checkPath(/koszyk\,dostawa\.html/, () => {
    paymentModifierObject.subscribe(() => {
      paymentModifierObject.applyChanges();
      paymentModifierObject.applyStyles();
    });
  });

  checkPath(/koszyk\,przeslij\.html/, () => {
    paymentModifierObject.subscribe(() => {
      paymentModifierObject.applyChanges();
      paymentModifierObject.applyStyles();
    });
  });
}
