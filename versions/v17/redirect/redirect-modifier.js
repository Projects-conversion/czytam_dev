import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class RegisterModifier extends ModifierSubject {
    constructor() {
        super('form[action="koszyk,dostawa.html"], form[action="/rejestracja.html"]', (that) => {});
    }

    applyChanges() {
        if(~jQuery('#main > .row > .small-10.columns:nth-child(1) > .legend').text().indexOf('zamawiającego')){
            this.applyStyles();
            window.location.replace("https://czytam.pl/koszyk,dostawa.html");
        }
    }

    applyStyles(){
        require('./redirect.scss');
    }

    redirect(){
        awaitElements('form[action="koszyk,dostawa.html"]').then(elem => {
            elem.attr('action','https://czytam.pl/koszyk,dostawa.html');
        });
    }
}
const registerModifier = new RegisterModifier();
export default registerModifier;