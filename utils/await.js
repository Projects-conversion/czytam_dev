export default function awaitElement(query, callback){
    const res = document.querySelectorAll(query);
    if(res.length > 0) {
        callback(res);
    } else {
       setTimeout(()=>{ awaitElement(query, callback); }, 10); 
    }
}