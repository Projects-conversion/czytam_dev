import ListingObject from './listing/listing-modifier';
import TopObject from './top/top-modifier';
import KpObject from './kp/kp-modifier';

export default function runGlobalPage() {

    ListingObject.subscribe(() => {
        ListingObject.applyChanges();
    });

    TopObject.subscribe(() => {
        TopObject.applyStyles();
        TopObject.applyChanges();
    });

    KpObject.subscribe(() => {
        KpObject.applyChanges();
        KpObject.applyStyles();
    });
}
