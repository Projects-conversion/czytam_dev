import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBarObject from '../progressbar/progressbar-modifier';
import SSLHeader from '../ssl-header/ssl-header-modifier';

class SummaryModifier extends ModifierSubject {
    constructor() {
        super('#zgoda', (that) => {});
    }

    applyChanges(){
        SSLHeader.applyChanges();
        ProgressBarObject.applyChanges(3, '#order_step');
    }

    applyStyles(){
        require('./summary.scss');
    }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;