import v9 from "../v9";
import v10 from "../v10";
import v11 from "../v11";

export default function run() {
  v9();
  v10();
  v11();
}
