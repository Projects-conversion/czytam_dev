import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import SSLTemplate from "./ssl-template.html";

class SSLModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.addSSL();
    this.applyStyles();
  }
  applyChangesRight() {
    this.addSSLRight();
    this.applyStylesRight();
  }

  applyStyles() {
    require("./ssl.scss");
  }

  applyStylesRight() {
    require("./ssl-right.scss");
  }

  addSSLRight() {
    return awaitElements(".small-9.columns.text-right.show-for-large-up").then(
      elem => {
        if (this.jQuery(".jbSSL").length == 0) {
          elem.after(SSLTemplate);
        }
      }
    );
  }

  addSSL() {
    return awaitElements("#header-logo .small-7.columns").then(elem => {
      if (this.jQuery(".jbSSL").length == 0) {
        elem.append(SSLTemplate);
      }
    });
  }
}
const sslModifier = new SSLModifier();
export default sslModifier;
