import awaitElement from './await';

export default function guard(query, callback, config = { attributes: true, childList: true, characterData: true} ){
	var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
	awaitElement(query, (targets)=>{
		let target = targets[0];
		//inform that target has value
		callback(target); 

		// create an observer instance
		let observer = new MutationObserver(function(mutations) {
			callback(target);  
		});

		// pass in the target node, as well as the observer options
		observer.observe(target, config);
		 
		// later, you can stop observing
		//observer.disconnect();

	});
	
}