import kpObject from "./kp/kp-modifier";
import BasketObject from "./basket/basket-modifier";
import pathChecker from "../../utils/pathChecker";

export default function runGlobalPage() {
  kpObject.subscribe(() => {
    kpObject.applyStyles();
    kpObject.applyChanges();
  });

  pathChecker(/koszyk.html/, () => {
    BasketObject.subscribe(() => {
      BasketObject.applyStyles();
      BasketObject.applyChanges();
    });
  });
}
