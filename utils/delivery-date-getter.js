export function getTextForDelivery() {
    const cDate = new Date();
    const timetoCheck = new Date(1970, 0, 1, cDate.getHours(), cDate.getMinutes());
    const timeBreakPoint = new Date(1970, 0, 1, 15, 0);

    switch (cDate.getDay()) {
        case 0:
            return `Zamów teraz a Twoje zamówienie<br> wyślemy już w poniedziałek!`
        case 1:
        case 2:
        case 3:
        case 4:
            return (timetoCheck <= timeBreakPoint) ? `Zamów teraz a Twoje zamówienie<br> wyślemy jeszcze dzisiaj!` :
                `Zamów teraz a Twoje zamówienie<br> wyślemy już jutro!`;
        case 5:
        return (timetoCheck <= timeBreakPoint) ? `Zamów teraz a Twoje zamówienie<br> wyślemy jeszcze dzisiaj!` :
                `Zamów teraz a Twoje zamówienie<br> wyślemy już w poniedziałek`
        case 6:
            return `Zamów teraz a Twoje zamówienie<br> wyślemy już w poniedziałek!`
    }
}

export function getTextForMobileDelivery() {
    const cDate = new Date();
    const timetoCheck = new Date(1970, 0, 1, cDate.getHours(), cDate.getMinutes());
    const timeBreakPoint = new Date(1970, 0, 1, 15, 0);

    switch (cDate.getDay()) {
        case 0:
            return `Zamów teraz a Twoje<br> zamówienie wyślemy<br> już w poniedziałek!`
        case 1:
        case 2:
        case 3:
        case 4:
            return (timetoCheck <= timeBreakPoint) ? `Zamów teraz a Twoje<br> zamówienie wyślemy jeszcze dzisiaj!` :
                `Zamów teraz a Twoje<br> zamówienie wyślemy<br> już jutro!`;
        case 5:
        return (timetoCheck <= timeBreakPoint) ? `Zamów teraz a Twoje<br> zamówienie wyślemy<br> jeszcze dzisiaj!` :
                `Zamów teraz a Twoje<br> zamówienie wyślemy<br> już w poniedziałek`;
        case 6:
            return `Zamów teraz a Twoje<br> zamówienie wyślemy<br> już w poniedziałek!`
    }
}