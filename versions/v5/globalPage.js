import kpObject from './kp/kp-modifier';
import BasketObject from './basket/basket-modifier';
import StatusObject from './status/status-modifier';
import pathChecker from '../../utils/pathChecker';

export default function runGlobalPage() {

    kpObject.subscribe(() => {
        kpObject.applyStyles();
        kpObject.applyChanges();
    });

    pathChecker(/koszyk.html/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyStyles();
            BasketObject.applyChanges();
        });
    })

    pathChecker(/koszyk,zamawiajacy|rejestracja|koszyk,dostawa|koszyk,przeslij/, () => {
        StatusObject.subscribe(() => {
            StatusObject.applyStyles();
            StatusObject.applyChanges();
        });
    })
}