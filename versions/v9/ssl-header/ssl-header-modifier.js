import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import SSLHeader from './ssl-header-template.html';

class ProgressBarModifier extends ModifierSubject {
    constructor() {
        super('.header_wrapper', (that) => {});
    }

    insertSSLHeader(){
        awaitElements('#header-logo > div:nth-child(1)').then(elem => {
            elem.after(SSLHeader);
        });
    }

    applyChanges(){
        this.insertSSLHeader();
        this.applyStyles();
    }

    applyStyles(){
        require('./ssl-header.scss');
    }
}
const progressBarModifierObject = new ProgressBarModifier();
export default progressBarModifierObject;