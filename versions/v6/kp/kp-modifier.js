import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import kpV4Object from "../../v4/kp/kp-modifier";
import kpV5Object from "../../v5/kp/kp-modifier";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".row.productreader", that => {});
  }

  applyChanges() {
    this.applyV5Changes();
    this.applyV4Changes();
  }

  applyStyles() {
    kpV4Object.applyStyles();
    kpV5Object.applyStyles();

    require("./kp.scss");
  }

  applyV5Changes() {
    kpV5Object.addDeliveryDateInfo();
    kpV5Object.addNewPriceContent();
    kpV5Object.changeGallery();
    kpV5Object.goToBasket();
    kpV5Object.changePromoInfo();
    kpV5Object.changeButton();
  }

  applyV4Changes() {
    kpV4Object.addNewDeliveryInfo();
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
