

import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import kpV16Object from '../../v16/kp/kp-modifier';

class KpModifier extends ModifierSubject {
    constructor() {
        super(".productreader", that => { });
    }

    applyChanges(callback = () => {}) {
        awaitElements(".tab-gallery > dl > dd").then(elem => {
          if (elem.length < 4) {
                kpV16Object.moveNavlement();
                kpV16Object.moveTabGallery();
                kpV16Object.applyStyles();
                callback();
            }
            else{
               // kpV16Object.moveNavlement();
            }
        });
    }

    applyStyles() {
        //require("./usp.scss");
    }
}
const kpModifier = new KpModifier();
export default kpModifier;