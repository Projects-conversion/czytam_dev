import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";

export default function run(){
    let device = new Device();
    device.setAllowed('mobile');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v1')){
        console.log("dziala wersja v1");
        window.runningVersionSS = 'v1'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v1');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}