import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import CartIcon from '../../../assets/sprint3/v3/cart-icon.svg';
import SearchIcon from '../../../assets/sprint3/search.svg';

import TopTemplate from './top-template.html';

class TopModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges(){
        this.insertTop();
        this.moveTabs();
        this.addDeliveryToFooter();
    }

    applyStyles(){
        require('./top.scss');
    }

    insertTop(){
        awaitElements('#header-logo').then(elem => {
            const template = jQuery(TopTemplate);
            const cart = jQuery('.link-cart');
            cart.find('img').attr('src', CartIcon);
            cart.addClass('jbCart')
            const search = jQuery('#header-nav-mobile > ul > li:nth-child(3) a').addClass('jbSearch');
            search.find('img').attr('src',SearchIcon);
            template.find('.jbMenu').after(search);
            template.find('.jbAccount').before(cart);
            elem.before(template);
        });
    }

    moveTabs(){
        Promise.all([
            awaitElements('#header-tab .row .tabs'),
            awaitElements('#header-slider')
        ]).then(([elemRef, elem]) => {
            elem.prepend(elemRef);
        });
    }

    addDeliveryToFooter(){
        awaitElements('#footer-nav nav ul:nth-child(1)').then(elem => {
            elem.prepend(`<li><a href="/dostawa.html" title="Książki">Koszty dostawy</a></li>`)
        });
    }
}
const topModifier = new TopModifier();
export default topModifier;