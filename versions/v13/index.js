import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";
import runv6 from '../v6';

export default function run(){
    let device = new Device();
    device.setAllowed('desktop');
    runv6();
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v13')){
        console.log("dziala wersja v13");
        window.runningVersionSS = 'v13'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v13');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}