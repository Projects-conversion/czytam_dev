import KpObject from './kp/kp-modifier';
import BasketObject from './basket/basket-modifier';
import StatusObject from '../v5/status/status-modifier';
import pathChecker from '../../utils/pathChecker';

export default function runGlobalPage() {

    KpObject.subscribe(() => {
        KpObject.applyStyles();
        KpObject.applyChanges();
    })

    pathChecker(/koszyk.html/, () => {
        BasketObject.subscribe(() => {
            BasketObject.applyStyles();
            BasketObject.applyChanges();
        });
    })

    pathChecker(/koszyk,zamawiajacy|rejestracja|koszyk,dostawa|koszyk,przeslij/, () => {
        StatusObject.subscribe(() => {
            StatusObject.applyStyles();
            StatusObject.applyChanges();
        });
    })
}