import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBarObject from '../progressbar/progressbar-modifier';
import SSLHeader from '../ssl-header/ssl-header-modifier';

class RegisterModifier extends ModifierSubject {
    constructor() {
        super('form[action="/rejestracja.html"]', (that) => { });
    }

    applyChanges() {
        SSLHeader.applyChanges();
        ProgressBarObject.applyChanges(1, 'section > div > div:nth-child(2)');
    }

    applyStyles() {
        require('./register.scss');
    }
}
const registerModifier = new RegisterModifier();
export default registerModifier;