import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import CompanyCheckobx from "./company-checkbox.html";

class DeliveryModifier extends ModifierSubject {
  constructor() {
    super(
      '#fdane1, form[action="koszyk,zamawiajacy.html"]',
      that => { }
    );
  }

  applyChanges() {
    this.addClassForMoveCheckbox();
    this.addCompanyCheckbox().then(() => {
      this.clickCheckboxes();
    });
    this.fillOthersInputs();
  }

  applyStyles() {
    require("./delivery.scss");
  }

  addCompanyCheckbox() {
    return awaitElements("#main > form .columns .legend").then(elems => {
      elems.each((i, o) => {
        const legend = jQuery(o);
        if (legend.text().trim() == "Dane do faktury") {
          legend.parent().before(CompanyCheckobx);
          legend
            .parent()
            .addClass("jbHidenInputsCompany")
            .addClass("jbHidden");
        } else if (legend.text().trim() == "Adres dostawy") {
          legend
            .parent()
            .addClass("jbHidenInputsDelivery")
            .addClass("jbHidden");
        } else if (
          legend
            .find("strong")
            .text()
            .trim() == "Adres do faktury"
        ) {
          legend.addClass("jbLegend");
        }
        else if (
          legend
            .text()
            .trim() == "Dane zamawiającego"
        ) {
          legend.addClass("jbLegend2");
        }
        this.toggleClasses(["#nip", "#firma"], "jbHidenInputsCompany");
        this.toggleClasses(
          [
            "#d_nazwa",
            "#d_ulica",
            "#d_nr_domu",
            "#d_miejscowosc",
            "#d_kod_pocztowy",
            "#d_kraj"
          ],
          "jbHidenInputsDelivery"
        );
      });
    });
  }

  clickCheckboxes() {
    awaitElements("#uniform-przenies").then(elem => {
      jQuery("#uniform-przenies input").click();
      jQuery("#uniform-przenies input").click();
      jQuery("#uniform-przenies input").click();
      jQuery("#uniform-przenies").click(() => {
        jQuery(".jbHidenInputsDelivery").toggleClass("jbHidden");
      });
    });

    awaitElements(`#company`).then(elem => {
      jQuery("#company").click(() => {
        jQuery(".jbHidenInputsCompany").toggleClass("jbHidden");
      });
    });
  }

  toggleClasses(elems, classs) {
    elems.forEach(elem => {
      jQuery(elem)
        .parent()
        .parent()
        .addClass("jbHidden")
        .addClass(classs);
    });
  }

  addClassForMoveCheckbox() {
    awaitElements("#uniform-przenies").then(elem => {
      elem.parent().addClass("jbCheckboxChange");
      // elem.click(() => {});
    });
  }

  fillOthersInputs() {
    return awaitElements('input.submit-next').then(elem => {
      elem.click(() => {

        if (jQuery('#uniform-przenies span').hasClass('checked') != false) {
          const firstLastName = `${jQuery('input[name="imie"]').val()} ${jQuery('input[name="nazwisko"]').val()}`;
          const street = `${jQuery('input[name="ulica"]').val()}`;
          const homeNo = `${jQuery('input[name="nr_domu"]').val()}`;
          const flatNo = `${jQuery('input[name="nr_mieszkania"]').val()}`;
          const town = `${jQuery('input[name="miejscowosc"]').val()}`;
          const code = `${jQuery('input[name="kod_pocztowy"]').val()}`;

          jQuery('textarea[name="d_nazwa"]').val(firstLastName);
          jQuery('input[name="d_ulica"]').val(street);
          jQuery('input[name="d_nr_domu"]').val(homeNo);
          jQuery('input[name="d_nr_mieszkania"]').val(flatNo);
          jQuery('input[name="d_miejscowosc"]').val(town);
          jQuery('input[name="d_kod_pocztowy"]').val(code);
        }
      })
    });
  }

}
const deliveryModifier = new DeliveryModifier();
export default deliveryModifier;
