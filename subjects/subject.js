export class Subject {
    constructor() {
        this.observers = [];
    }

    notify(...args) {
        this.observers.forEach((observer) => {
            observer(...args);
        });
    }

    subscribe(observer) {
        if (typeof observer === 'function') {
            this.observers.push(observer);
            return () => {
                const idx = this.observers.indexOf(observer);
                if (idx > -1) {
                    this.observers.splice(idx, 1);
                }
            };
        }
    }
}