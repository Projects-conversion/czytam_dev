import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class ListingModifier extends ModifierSubject {
    constructor() {
        super('.productreader', (that) => {});
    }

    applyChanges(){
        this.addBreadcrumb();
    }

    applyStyles(){
        require('./kp.scss');
    }

    addBreadcrumb(){
        awaitElements('#breadcrumb').then(elem => {
            const current = elem.find('li');
            const template = jQuery(`<div class="jbBreadcrumb"></div>`);
            template.prepend(jQuery(current[current.length - 2]).find('a'));
            jQuery('.productreader').before(template);
        });
    }
}
const listingModifier = new ListingModifier();
export default listingModifier;