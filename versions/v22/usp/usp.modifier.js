import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import UspTemplate from "./usp.html";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.addUsp();
  }

  addUsp() {
    awaitElements("header").then(elem => {
      elem.before(UspTemplate);
    });
  }

  applyStyles() {
    require("./usp.scss");
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
