import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import KpV16Object from '../../v16/kp/kp-modifier';
import UspObject from '../usp/usp.modifier';

import DeliveryIcon from '../../../assets/sprint2/v5/truck_small.svg';

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => { });
  }

  applyChanges() {
    awaitElements(".tab-gallery > dl > dd").then(elem => {
      if (elem.length < 4) {
        this.applyStyles();
        this.applyChangesV16();
        this.changeButtonText();
        this.addRedirectToBasket();
        this.addDelivery();
        this.moveDescription();
        this.wrapDescription();
        this.applyUspV22();
      }
    });
    
    awaitElements('.jbNewDescription.jbWrapText iframe').then(elem => {
      //debugger
        //if(elem.text() == "Nóż "){
          awaitElements('.jbNewDescription').then(elem => elem.addClass('jbSpecialWrapText'));
       // }
    });
  }

  applyChangesV16() {
    KpV16Object.applyChanges();
  }

  applyUspV22() {
    UspObject.applyStyles();
    UspObject.applyChanges();
  }
  moveDescription() {
    Promise.all([
      awaitElements('#opis > blockquote:nth-child(2)'),
      awaitElements('.parametry.parametry-top')
    ]).then(([desc, elemRef]) => {

      if(jQuery('#opis > blockquote:nth-child(2) .csDBHtmlContentPresenter').length >0){
        elemRef.after(jQuery('#opis > blockquote:nth-child(2) .csDBHtmlContentPresenter').addClass('jbNewDescription'));
      }else{
        desc.each((i, obj) =>{
          if(this.jQuery(obj).text() != ''){
            elemRef.after(jQuery(obj).addClass('jbNewDescription'));
            
          }
        });
      }
      
    })
  }

  applyStyles() {
    require("./kp.scss");
  }

  wrapDescription() {
    Promise.all([
      awaitElements('.jbGallery'),
      awaitElements('#main > .row.productreader > .small-9.medium-11.columns')
    ]).then(([gallery, desc]) => {
      let descriptionHeight =
        jQuery('.row.productreader > .small-9.medium-11.columns > .show-for-medium-up').height() +
        jQuery('.row.productreader > .small-9.medium-11.columns > .row > .small-16.medium-8.columns').height();

      if (descriptionHeight >= (gallery.height())) {
        jQuery('.jbNewDescription').addClass('jbWrapText');
        const wrapTemplate = jQuery(`<div class="jbWrap"><span>Rozwiń opis</span></div>`);
        wrapTemplate.find('span').click(() => {
          if (jQuery('.jbNewDescription.jbWrapText').length > 0) {
            jQuery('.jbNewDescription').removeClass('jbWrapText');
            wrapTemplate.addClass('jbNoWrap');
            wrapTemplate.find('span').text('Zwiń opis');
          } else {
            jQuery('.jbNewDescription').addClass('jbWrapText');
            wrapTemplate.removeClass('jbNoWrap');
            wrapTemplate.find('span').text('Rozwiń opis');
          }
        });
        this.jQuery('.jbNewDescription').after(wrapTemplate);
      }
    });
  }

  changeButtonText() {
    awaitElements('.add-cart-big span').then(elem => {
      elem.text('Dodaj do koszyka');
    });
  }

  addRedirectToBasket() {
    awaitElements('.add-cart-big').then(elem => {
      elem.click(() => {
        if (jQuery('.jbRedirectToBasket').length == 0) {
          elem.after(`<a href="https://czytam.pl/koszyk.html"><span class="jbRedirectToBasket">Przejdź do koszyka</span></a>`);
          elem.addClass('jbAddButton');
          jQuery('#schowek').addClass('jbHide');
        }
      })
    });
  }

  addDelivery() {
    awaitElements('.wysylka').then(elem => {
      elem.addClass('jbHide');
      const selector = (jQuery('.add-cart-big').length > 0) ? jQuery('.add-cart-big') : jQuery('#schowek').children();
      let deliveryText = elem.find('strong').text();
      if (~deliveryText.indexOf('(')) {
        deliveryText = deliveryText.substring(0, elem.find('strong').text().indexOf('('));
      }
      selector.parent().before(`<div class="jbWysylka">
            <img src="${DeliveryIcon}"/>
            <span>Wysyłka: ${deliveryText}</span>
          </div>`);
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
