import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

class PaymentModifier extends ModifierSubject {
  constructor() {
    super(".inside-step-shipping", that => {});
  }

  applyChanges() {
    this.removeStep();
    this.changeHeadlineStep();
    this.addScroll();
    this.replaceLabelsPayment();
  }

  applyStyles() {
    require("./payment.scss");
  }

  removeStep() {
    awaitElements(".inside-step-shipping ").then(elem => {
      elem
        .contents()
        .filter(function() {
          return this.nodeType == 3; //Node.TEXT_NODE
        })
        .remove();
    });
  }

  changeHeadlineStep() {
    awaitElements('form[action="koszyk,przeslij.html"] > div > h3').then(
      elem => {
        elem.addClass("jbHidden");
        elem.before(`<h3>Wybierz rodzaj płatności i dostawy</h3>`);
      }
    );
  }

  addScroll() {
    awaitElements("#info_1_10").then(elem => {
      elem
        .parent()
        .find("> label")
        .click(() => {});
    });
  }
  replaceLabelsPayment() {
    awaitElements(".table-shipping td >label").then(elems => {
      elems.each((i, o) => {
        const label = jQuery(o);
        label.find("i").before(label.find("strong"));
        label
          .find("strong:first-of-type")
          .after(label.find("br:first-of-type"));
        label.find("i").before("<br>");
        label
          .contents()
          .filter(function() {
            return this.nodeType == 3; //Node.TEXT_NODE
          })
          .remove();
        label.append(label.find("span.red"));

        const firstChildOfNode = label.contents().get(0);
        if (firstChildOfNode.nodeName == "BR") {
          firstChildOfNode.remove();
        }
      });
    });
  }
}
const paymentModifier = new PaymentModifier();
export default paymentModifier;
