import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBarObject from '../progressbar/progressbar-modifier';
import SSLHeader from '../ssl-header/ssl-header-modifier';

class OrdererModifier extends ModifierSubject {
    constructor() {
        super('#fdane1, form[action="koszyk,dostawa.html"]', (that) => {});
    }

    applyChanges(){
        SSLHeader.applyChanges();
        ProgressBarObject.applyChanges(1, '#order_step');
    }

    applyStyles(){
        require('./orderer.scss');
    }
}
const ordererModifier = new OrdererModifier();
export default ordererModifier;