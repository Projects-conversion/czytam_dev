import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class StatusModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges() {
       
    }

    applyStyles() {
        require('./status.scss');
    }


}
const statusModifier = new StatusModifier();
export default statusModifier;