import pathChecker from '../../utils/pathChecker';

import SummaryObject from './summary/summary-modifier';

export default function runGlobalPage() {

    pathChecker(/koszyk,przeslij/, () => {
        SummaryObject.subscribe(() => {
            SummaryObject.applyStyles();
            SummaryObject.applyChanges();
        });
    });
}