import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";
import runv11 from '../v11';

export default function run(){
    let device = new Device();
    device.setAllowed('desktop');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    runv11();
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v24')){
        console.log("dziala wersja v24");
        window.runningVersionSS = 'v24'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v24');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}