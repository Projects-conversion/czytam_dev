import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

class RegisterModifier extends ModifierSubject {
  constructor() {
    super('form[action="/rejestracja.html"]', that => {});
  }

  applyChanges() {

  }

  applyStyles() {
    require("./register.scss");
  }
}
const registerModifier = new RegisterModifier();
export default registerModifier;
