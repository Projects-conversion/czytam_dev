import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import progressbarObject from "../progressbar/progressbar-modifier";
import SSLObject from "../ssl/ssl-modifier";
import CompanyCheckobx from "./company-checkbox.html";

class RegisterModifier extends ModifierSubject {
  constructor() {
    super('form[action="koszyk,zamawiajacy.html"]', that => {});
  }

  applyChanges() {
    if (
      ~jQuery("#main > form > .row > .small-10.columns > .legend")
        .text()
        .indexOf("zamawiającego")
    ) {
      this.applyStyles();
      progressbarObject.addProgressBar(
        1,
        ".small-16.large-12.xlarge-13.columns #main",
        false
      );
      
      //this.jQuery('#company').parent().addClass('jbChecked');
      this.fillOthersInputs();
      SSLObject.applyChanges();
      this.redirect();
      this.addCompanyCheckbox();
      this.clickCheckboxes();
      this.checkCheckbox();
      this.handleOneLineCountry();
      this.handleOneLineCountry("#d_kod_pocztowy", "jbCode", "#d_kraj");
    }
  }

  applyStyles() {
    require("./register.scss");
  }

  fillOthersInputs() {
    return awaitElements("input.submit-next").then(elem => {
      elem.click(() => {
        if (jQuery("#uniform-przenies span").hasClass("checked") == false) {
          const firstLastName = `${jQuery('input[name="imie"]').val()} ${jQuery(
            'input[name="nazwisko"]'
          ).val()}`;
          const street = `${jQuery('input[name="ulica"]').val()}`;
          const homeNo = `${jQuery('input[name="nr_domu"]').val()}`;
          const flatNo = `${jQuery('input[name="nr_mieszkania"]').val()}`;
          const town = `${jQuery('input[name="miejscowosc"]').val()}`;
          const code = `${jQuery('input[name="kod_pocztowy"]').val()}`;

          jQuery('textarea[name="d_nazwa"]').val(firstLastName);
          jQuery('input[name="d_ulica"]').val(street);
          jQuery('input[name="d_nr_domu"]').val(homeNo);
          jQuery('input[name="d_nr_mieszkania"]').val(flatNo);
          jQuery('input[name="d_miejscowosc"]').val(town);
          jQuery('input[name="d_kod_pocztowy"]').val(code);
        }
      });
    });
  }

  redirect() {
    awaitElements('form[action="koszyk,zamawiajacy.html"]').then(elem => {
      elem.attr("action", "https://czytam.pl/koszyk,dostawa.html");
    });
  }

  addCompanyCheckbox() {
    return awaitElements("#main > form .columns .legend").then(elems => {
      elems.each((i, o) => {
        const legend = jQuery(o);
        if (legend.text().trim() == "Dane do faktury") {
          legend
            .parent()
            .prev()
            .before(CompanyCheckobx);
          legend
            .parent()
            .addClass("jbHidenInputsCompany")
            .addClass("jbHidden");
        } else if (legend.text().trim() == "Adres dostawy") {
          legend.addClass("jbDeliveryAd");
          legend
            .parent()
            .addClass("jbHidenInputsDelivery")
            .addClass("jbHidden");
        } else if (
          legend
            .find("strong")
            .text()
            .trim() == "Adres do faktury"
        ) {
          legend.parent().addClass("jbLegend");
        }
        this.toggleClasses(["#nip", "#firma"], "jbHidenInputsCompany");
        jQuery(".jbHidenInputsCompany")
          .eq(2)
          .next()
          .next()
          .addClass("jbHidden")
          .addClass("jbHidenInputsCompany");
        this.toggleClasses(
          [
            "#d_nazwa",
            "#d_ulica",
            "#d_nr_domu",
            "#d_miejscowosc",
            "#d_kod_pocztowy",
            "#d_kraj"
          ],
          "jbHidenInputsDelivery"
        );
      });
    });
  }

  toggleClasses(elems, classs) {
    elems.forEach(elem => {
      jQuery(elem)
        .parent()
        .parent()
        .addClass("jbHidden")
        .addClass(classs);
    });
  }
  clickCheckboxes() {
    awaitElements("#uniform-przenies").then(elem => {
      jQuery("#uniform-przenies").click(() => {
        jQuery(".jbHidenInputsDelivery").toggleClass("jbHidden");
      });
    });
  }

  checkCheckbox(){
    awaitElements(`#company`).then(elem => {
      jQuery("#company").click(() => {
        jQuery(".jbHidenInputsCompany").toggleClass("jbHidden");
      });
    });
  }

  handleOneLineCountry(
    ref = "#kod_pocztowy",
    classref = "jbCode",
    toChange = "#kraj"
  ) {
    awaitElements(ref).then(elem => {
      elem
        .parent()
        .parent()
        .addClass(classref);
      elem
        .parent()
        .parent()
        .append(jQuery(toChange).parent());
    });
  }
}
const registerModifier = new RegisterModifier();
export default registerModifier;
