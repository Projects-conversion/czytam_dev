import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import ProgressBar from './progressbar-template.html';

class ProgressBarModifier extends ModifierSubject {
    constructor() {
        super('.header_wrapper', (that) => {});
    }

    applyChanges(step = 1, selector = '#breadcrumbs', isLoginPage = false) {
        this.addProgressBar(step, selector, isLoginPage);
    }

    addProgressBar(step, selector, isLoginPage) {
        awaitElements(selector).then(element => {
            jQuery(element).before(ProgressBar);
            this.setActiveStep(step);
        }).catch(error => console.log(error));
    }

    getProgressBar() {
        return jQuery(ProgressBar);
    }

    setActiveStep(stepNr) {
        let activeStep;
        let activeLine;
        let currentStep;
        switch (stepNr) {
            case 1:
                currentStep = jQuery('#jbStepOne');
                activeStep = jQuery('#jbStepOne');
                activeLine = jQuery('#jbLineOne');
                break;
            case 2:
                currentStep = jQuery('#jbStepTwo');
                activeStep = jQuery('#jbStepTwo');
                activeLine = jQuery('#jbLineOne, #jbLineTwo');
                break;
            case 3:
                currentStep = jQuery('#jbStepThree');
                activeStep = jQuery('#jbStepThree');
                activeLine = jQuery('#jbLineOne, #jbLineTwo, #jbLineThree');
                break;
            case 4:
                currentStep = jQuery('#jbStepFour');
                activeStep = jQuery('#jbSteepFour');
                activeLine = jQuery('#jbLineOne, #jbLineTwo, #jbLineThree, #jbLineFour');
                break;
        }

        this.setRefLinkToBackStep(stepNr);

        activeStep.removeClass('jbProgressInactiveStep').addClass('jbProgressActiveStep');
        activeLine.removeClass('jbProgressLineEmpty').addClass('jbProgressLineFilled');
        currentStep.addClass('jbProgressCurrentStep');
    }

    setRefLinkToBackStep(stepNr){
        switch(stepNr) {
            case 2:
               // jQuery("#jbFirstHref").attr('href', 'https://czytam.pl/koszyk,zamawiajacy.html?req=true');
               jQuery("#jbFirstHref").attr('href', 'https://czytam.pl/koszyk,zamawiajacy.html');
                 
               jQuery("#jbFirstHref").removeClass('jbNoClick');
                break;
            case 3:
                //jQuery("#jbFirstHref").attr('href', 'https://czytam.pl/koszyk,zamawiajacy.html?req=true');
                jQuery("#jbFirstHref").attr('href', 'https://czytam.pl/koszyk,zamawiajacy.html');
                
                jQuery("#jbSecondHref").attr('href', 'https://czytam.pl/koszyk,dostawa.html');
                jQuery("#jbFirstHref").removeClass('jbNoClick');
                jQuery("#jbSecondHref").removeClass('jbNoClick');
                break;
            
            default:
                break;
        }
    }
}
const progressBarModifierObject = new ProgressBarModifier();
export default progressBarModifierObject;