import {jQuerySubject} from '../subjects';

export class Modifier {
    constructor(selector, callback) {
        jQuerySubject.subscribe((jQuery)=>{
            this.jQuery = jQuery;
            jQuery(document).ready(()=>{
                this.domReady = true;
            });
            this.wait(selector,callback);
        });
    }

    wait(selector, callback){
        this.modifiedElement = this.jQuery(selector);
        if(this.modifiedElement.length === 0){
            if(!this.domReady){
                setTimeout(()=>{
                    this.wait(selector,callback);
                }, 100);
            }
        } else {
            if (callback) callback(this);
            this.applyChanges();
        }
    }

    applyChanges(){}
}