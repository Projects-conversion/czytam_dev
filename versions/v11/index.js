import { LoadingScreen } from "../../utils/loading-screen";
import { Device } from "../../utils/device";
import runGlobalPage from "./globalPage";
import v6 from "../v6";

export default function run() {
  let device = new Device();
  device.setAllowed("mobile");
  v6();
  window.runningVersionSSarr = window.runningVersionSSarr || [];
  if (device.isValid() && !~window.runningVersionSSarr.indexOf("v11")) {
    console.log("dziala wersja v11");
    window.runningVersionSS = "v11"; //for page quasii full reloading checking
    window.runningVersionSSarr.push("v11");
    const ls = new LoadingScreen();
    runGlobalPage();
  }
}
