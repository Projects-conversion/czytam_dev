import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

import progressbarObject from "../progressbar/progressbar-modifier";
import sslObject from "../ssl/ssl-modifier";

class LoginModifier extends ModifierSubject {
  constructor() {
    super('#main > form[action="koszyk,zamawiajacy.html"]', that => {});
  }
  applyChanges() {
    if (
      ~jQuery('form[action="koszyk,zamawiajacy.html"] label:nth-child(1)')
        .text()
        .indexOf("klientem")
    ) {
      progressbarObject.addProgressBar(
        1,
        ".small-16.large-12.xlarge-13.columns #main",
        false
      );
      sslObject.applyChanges();
      this.applyStyles();
    }
  }

  applyStyles() {
    require("./login.scss");
  }
}
const loginModifier = new LoginModifier();
export default loginModifier;
