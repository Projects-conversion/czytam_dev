import HomePageObject from './home-page/home-page-modifier';
import ListingObject from './listing/listing-modifier';

export default function runGlobalPage() {

    if(window.location.href == 'https://czytam.pl/'){
        HomePageObject.subscribe(() => {
            HomePageObject.applyStyles();
            HomePageObject.applyChanges();  
        })
    };

    ListingObject.subscribe(() => {
        ListingObject.applyStyles();
        ListingObject.applyChanges();
    })
}