import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import UspTemplate from '../../v13/home-page/usp-template.html';

class HomePageModifier extends ModifierSubject {
    constructor() {
        super('.headline.icon-katalog', (that) => {});
    }

    applyChanges(){
        if(jQuery('.productreader').length == 0){
            this.addUsp();
        }
    }

    applyStyles(){
        require('./listing.scss');
    }

    addUsp(){
        awaitElements('#left').then(elem => {
            elem.prepend(UspTemplate);
        });
    }
}
const homePageModifier = new HomePageModifier();
export default homePageModifier;