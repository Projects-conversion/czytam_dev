import { Device } from "../../utils/device";

import runv17 from "../v17";
import runv18 from "../v18";
import runv21 from "../v21";
import runv24 from "../v24";
import runv25 from "../v25";

export default function run() {
  let deviceD = new Device();
  let deviceM = new Device();
  deviceD.setAllowed("desktop");
  deviceM.setAllowed("mobile");

  window.runningVersionSSarr = window.runningVersionSSarr || [];

  if (!~window.runningVersionSSarr.indexOf("v29")) {
    console.log("dziala wersja v29");
    window.runningVersionSS = "v29"; //for page quasii full reloading checking
    window.runningVersionSSarr.push("v29");

    if (deviceM.isValid()) {
      runv17();
      runv18();
      runv21();
    } else if (deviceD.isValid()) {
      runv24();
      runv25();
    }
  }
}