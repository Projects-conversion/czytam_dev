import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

class BasketModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges() {
       this.changeHeader();
       this.checkLoginStatus();
    }

    applyStyles() {
        require('./basket.scss');
    }

    changeHeader(){
        return awaitElements('.headline-border-bottom').then(elem => {
            elem.after(`<div class="jbHeader"><span>Twój koszyk<span></div>`);
        });
    }

    insertSectionToLogin(isLogged = false){
        return awaitElements('#shipping-costs').then(elem => {
            let template = "";
            if(!isLogged){
                template = ` <div class="jbNoLogin">
                    <span>Posiadasz konto na Czytam.pl?</span>
                    <span><a href="https://czytam.pl/konto.html">Zaloguj się</a> aby wykorzystać rabat dla klientów</span>
                </div>`;
            }else{
                template = jQuery('.logout');
            }
            elem.before(template);
        }); 
    }

    checkLoginStatus(){
        return awaitElements('#header-account .link-registered').then(elem => {
            if(~elem.text().indexOf('Wyloguj')){
                this.insertSectionToLogin(true);
            }else{
                this.insertSectionToLogin();
            }
        });
    }
}
const basketModifier = new BasketModifier();
export default basketModifier;