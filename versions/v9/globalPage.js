import pathChecker from '../../utils/pathChecker';

import LoginObject from './login/login-modifier';
import WithRegisterObject from './with-register/register-modifier';
import OrdererObject from './orderer/orderer-modifier';
import DeliveryObject from './delivery/delivery-modifier';
import SummaryObject from './summary/summary-modifier';
import TotalSummaryObject from './total-summary/summary-modifier';

export default function runGlobalPage() {

    pathChecker(/koszyk,zamawiajacy.html/, () => {
        LoginObject.subscribe(() => {
            LoginObject.applyChanges();
            LoginObject.applyStyles();
        });
        pathChecker(/koszyk,zamawiajacy\.html$/, () => {
            OrdererObject.subscribe(() => {
                OrdererObject.applyStyles();
                OrdererObject.applyChanges();
            });
        });
    });

    pathChecker(/rejestracja/, () => {
        WithRegisterObject.subscribe(() => {
            WithRegisterObject.applyStyles();
            WithRegisterObject.applyChanges();
        });
    });

    pathChecker(/koszyk,dostawa|koszyk,przeslij/, () => {
        DeliveryObject.subscribe(() => {
            DeliveryObject.applyStyles();
            DeliveryObject.applyChanges();
        });
    });

    pathChecker(/koszyk,przeslij/, () => {
        SummaryObject.subscribe(() => {
            SummaryObject.applyStyles();
            SummaryObject.applyChanges();
        });

       /* TotalSummaryObject.subscribe(() => {
            TotalSummaryObject.applyStyles();
            TotalSummaryObject.applyChanges();
        });*/
    })

    pathChecker(/thankyou.html/, () => {
        TotalSummaryObject.subscribe(() => {
            TotalSummaryObject.applyStyles();
            TotalSummaryObject.applyChanges();
        });
    });
}