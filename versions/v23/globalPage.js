import kpObject from './kp/kp-modifier';
import basketObject from './basket/basket-modifier';
import pathChecker from "../../utils/pathChecker";

export default function runGlobalPage() {

    kpObject.subscribe(() => {
        kpObject.applyStyles();
        kpObject.applyChanges();
    });

    pathChecker(/koszyk.html/, () => {
        basketObject.subscribe(() => {
            basketObject.applyStyles();
            basketObject.applyChanges();
        })
    });
}