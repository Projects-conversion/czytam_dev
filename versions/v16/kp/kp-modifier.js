import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges(callback = () => {}) {
    awaitElements(".tab-gallery > dl > dd").then(elem => {
      if (elem.length < 4) {
        console.log(elem);
        this.moveNavlement();
        this.moveTabGallery();
        this.applyStyles();
        callback();
      }else{
        this.moveNavlement();
        this.applyStyles2();
      }
    });
  }

  applyStyles2(){
    require('./kp-modifier2.scss');
  }

  applyStyles() {
    require("./kp-modifier.scss");
  }
  moveNavlement() {
    awaitElements("#opis").then(elem => {
      elem.wrap('<div class="jbDescWrap row"></div>');
      jQuery(".jbDescWrap").prepend(jQuery("#left"));
      jQuery(".productreader").after(jQuery(".jbDescWrap"));
    });
  }

  moveTabGallery() {
    awaitElements(".tab-gallery").then(elem => {
      jQuery(".gallery")
        .parent()
        .prepend(elem)
        .addClass("jbGallery");

      jQuery(".tab-gallery, .gallery").wrapAll(
        `<div class="jbWrapGallery"></div>`
      );
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
