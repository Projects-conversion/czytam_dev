import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { createItemTemplate, addClickHandlers } from "./basket-helper";
import { ajaxSubject } from "../../../utils/ajax.subject";
import guard from "../../../utils/guard";

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges(shouldChangeInfoGratis = true) {
    if (shouldChangeInfoHeader) {
      this.changeHeader();
    }
    this.getProducts();
    this.updateProducts();
    this.insertTotalSummary();
    this.changeInfoGratis();
    this.moveAddInfo();
    this.shouldChangeInfoGratis = shouldChangeInfoGratis;
  }

  applyStyles() {
    require("./basket.scss");
  }

  changeHeader() {
    return awaitElements(".headline-border-bottom").then(elem => {
      elem.after(`<div class="jbHeader"><span>Twój koszyk<span></div>`);
    });
  }

  getProducts() {
    return awaitElements(".cart_summary tbody tr").then(elem => {
      let template = "";
      elem.each((i, obj) => {
        const object = jQuery(obj);
        template += createItemTemplate(object);
      });

      // addClickHandlers(jQuery(template).find('.jbBasket'));

      if (jQuery(".jbBasket").length == 0) {
        jQuery("#main").prepend(`<div class="jbBasket">${template}</div>`);
      }

      addClickHandlers(jQuery(".jbBasket"));
    });
  }

  insertTotalSummary() {
    return awaitElements(".jbBasket").then(elem => {
      if (jQuery(".jbTotalSummary").length == 0) {
        elem.after(`
                <div class="jbTotalSummary">
                    <span class="title">Wartość koszyka:</span>
                    <div class="price">
                        <span class="value">${jQuery(
                          ".cart_total_price strong"
                        ).text()}</span>
                        <span class="currency">PLN</span>
                    </div>
                </div>
                `);
      } else {
        jQuery(".jbTotalSummary .price .value").text(
          jQuery(".cart_total_price strong").text()
        );
      }
    });
  }

  changeInfoGratis() {
    return awaitElements("#info_gratis").then(elem => {
      const value = elem.text();
      let text = "";

      if (~value.indexOf("do bezpłatnej dostawy brakuje")) {
        var mySubString = value.substring(
          value.indexOf("brakuje") + 7,
          value.indexOf("PLN")
        );
        text = `<span>Do bezpłatnej dostawy brakuje <b>${mySubString}</b> PLN</span>`;
        elem.html(text);
      } else if (~value.indexOf("dostawa jest GRATIS")) {
        text = `<span>Dostawa jest bezpłatna</span>`;
        elem.html(text);
      }
    });
  }

  updateProducts() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        this.getProducts();
        guard("#main", elem => {
          //if(jQuery('.jbBasket').length == 0){
          this.getProducts();
          this.insertTotalSummary();

          if (this.shouldChangeInfoGratis) {
            this.changeInfoGratis();
          }
          //   }
        });
      }
    });
  }

  moveAddInfo() {
    return awaitElements("#main > form > .small-16.small-full.columns").then(
      elem => {
        jQuery('form[action="/koszyk,zamawiajacy.html"]')
          .parent()
          .after(elem.clone(true, true).addClass("jbInfo"));
        jQuery(".jbInfo").before(`<hr class="hr-3 jbHR3">`);
      }
    );
  }
}
const basketModifier = new BasketModifier();
export default basketModifier;
