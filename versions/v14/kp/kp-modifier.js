import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import promoInfor from "./promo-info.html";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".rectangle-promo", that => {});
  }

  applyChanges(callback = () => {}) {
    awaitElements(".tab-gallery > dl > dd").then(elem => {
      if (elem.length < 4) {
        this.addPromoInfo();
        this.applyStyles();
        callback();
      }
    });
  }

  applyStyles() {
    require("./kp-modifier.scss");
  }

  addPromoInfo() {
    awaitElements(".save").then(elem => {
      if(~jQuery('.rectangle-promo span').text().indexOf('Promo')){
        elem.after(promoInfor);
      }
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
