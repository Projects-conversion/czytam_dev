import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import promoInfor from "./promo-info.html";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.movePromoBadge();
    this.changePromoPriceText();
    this.addPromoInfo();
  }

  applyStyles() {
    require("./kp.scss");
  }

  movePromoBadge() {
    Promise.all([
      awaitElements(".rectangle-promo.orange"),
      awaitElements(".jbTotalPrice")
    ]).then(([orygBadge, container]) => {
      container.before(
        jQuery(orygBadge[0])
          .clone(true, true)
          .addClass("jbNewBadge")
      );
      orygBadge.addClass("jbHide");
    });
  }

  changePromoPriceText() {
    Promise.all([
      awaitElements(".rectangle-promo"),
      awaitElements(".jbNewPrice .title"),
      awaitElements(".jbOldPrice .title")
    ]).then(([orygBadge, newPrice, oldPrice]) => {

      if((~jQuery('#breadcrumb').text().indexOf('Książk')) || (~orygBadge.text().indexOf('Książk')) ){
        if (~orygBadge.find("span").text().indexOf("Promo")){
          oldPrice.text("Cena przed promocją");
        }
        else{
          oldPrice.text("Sugerowana cena detaliczna");
        }
      }else{
        if (~orygBadge.find("span").text().indexOf("Promo")){
          oldPrice.text("Cena przed promocją");
        }
        else{
          oldPrice.text("Sugerowana cena producenta");
        }
      }
      /*if (
        ~orygBadge
          .find("span")
          .text()
          .indexOf("Promo")
      ) {
        oldPrice.text("Cena przed promocją");
        newPrice.text("Aktualna cena");
      } else {
        if(~orygBadge.find('span').text().indexOf('Książka')){
          oldPrice.text("Sugerowana cena producenta");
        }
        else {
            oldPrice.text("Sugerowana cena detaliczna");
        }
        
      }*/
    });
  }

  addPromoInfo() {
    awaitElements(".jbDiscount").then(elem => {
      if (
        ~jQuery(".rectangle-promo span")
          .text()
          .indexOf("Promo")
      ) {
        elem.after(promoInfor);
        jQuery('.tab-gallery').addClass('jbPromoPadding');
      } else {
        jQuery(".tab-gallery").addClass("nonPromoGallery");
      }
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
