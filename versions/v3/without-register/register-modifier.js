import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import progressbarObject from '../progressbar/progressbar-modifier';
import SSLObject from '../ssl/ssl-modifier';

class RegisterModifier extends ModifierSubject {
    constructor() {
        super('form[action="koszyk,zamawiajacy.html"]', (that) => {});
    }

    applyChanges() {
        if(~jQuery('#main > form > .row > .small-10.columns > .legend').text().indexOf('zamawiającego')){
            this.applyStyles();
            progressbarObject.addProgressBar(1, '.small-16.large-12.xlarge-13.columns #main', false);
            this.showOrHideSecondAddress();
            this.fillOthersInputs();
            SSLObject.applyChanges();
            this.redirect();
        }
    }

    applyStyles() {
        require('./register.scss');
    }

    fillOthersInputs(){
        return awaitElements('input.submit-next').then(elem => {
            elem.click(() => {

                if(jQuery('#uniform-przenies span').hasClass('checked') == false){
                    const firstLastName = `${jQuery('input[name="imie"]').val()} ${jQuery('input[name="nazwisko"]').val()}`;
                    const street = `${jQuery('input[name="ulica"]').val()}`;
                    const homeNo = `${jQuery('input[name="nr_domu"]').val()}`;
                    const flatNo = `${jQuery('input[name="nr_mieszkania"]').val()}`;
                    const town = `${jQuery('input[name="miejscowosc"]').val()}`;
                    const code = `${jQuery('input[name="kod_pocztowy"]').val()}`;

                    jQuery('textarea[name="d_nazwa"]').val(firstLastName);
                    jQuery('input[name="d_ulica"]').val(street);
                    jQuery('input[name="d_nr_domu"]').val(homeNo);
                    jQuery('input[name="d_nr_mieszkania"]').val(flatNo);
                    jQuery('input[name="d_miejscowosc"]').val(town);
                    jQuery('input[name="d_kod_pocztowy"]').val(code);
                }    
            })
        });
    }

    redirect(){
        awaitElements('form[action="koszyk,zamawiajacy.html"]').then(elem => {
            elem.attr('action','https://czytam.pl/koszyk,dostawa.html');
        });
    }

    //TO REMOVE !!!!
    /*redirect(){
        const validity = jQuery('form[action="koszyk,zamawiajacy.html"]')[0].checkValidity();
                if(validity)
                {
                    console.log('jestem tutaj');
                    setTimeout(() =>{
                        $(location).prop('href', "https://czytam.pl/koszyk,dostawa.html");
                       // window.location.replace("https://czytam.pl/koszyk,dostawa.html");
                       // window.location.href = "https://czytam.pl/koszyk,dostawa.html";
                        //window.location = window.location.host;
                        //window.location.reload();
                    }, 1000)

                }else{
                    console.log('bad validity tutaj');
                }
    }*/

    showOrHideSecondAddress(){
        return awaitElements('#uniform-przenies').then(elem => {
            elem.find('#przenies').click(() => {
                const selectBox = elem.find('span');

                if(selectBox.hasClass('checked') == true){
                    jQuery('#main > form .row > div:nth-child(32)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(34)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(36)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(37)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(39)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(40)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(42)').addClass('jbShow');
                }else{
                    jQuery('#main > form .row > div:nth-child(32)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(34)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(36)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(37)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(39)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(40)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(42)').removeClass('jbShow');
                }
            });
        });
    }
}
const registerModifier = new RegisterModifier();
export default registerModifier;