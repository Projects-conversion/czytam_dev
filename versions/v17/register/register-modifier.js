import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import RegisterV9Object from '../../v9/with-register/register-modifier';
import RegisterV10Object from '../../v10/register/register-modifier';
import DeliveryV10Object from '../../v10/delivery/delivery-modifier';

class RegisterModifier extends ModifierSubject {
    constructor() {
        super('#fdane1, form[action="koszyk,zamawiajacy.html"], form[action="/rejestracja.html"]', (that) => {});
    }

    applyChanges(){
        this.applyChangesV9();
        this.applyChangesV10();
    }
    applyChangesV9(){
        RegisterV9Object.applyChanges();
        RegisterV9Object.applyStyles();
        RegisterV10Object.applyChanges();
        RegisterV10Object.applyStyles();
    }

    applyChangesV10(){
        DeliveryV10Object.applyChanges();
        this.applyStyles();
    }

    applyStyles(){
        require('./register.scss');
    }
}
const registerModifierObject = new RegisterModifier();
export default registerModifierObject;