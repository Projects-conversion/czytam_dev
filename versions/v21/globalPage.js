import uspObject from "./usp/usp";
import { jQuerySubject } from "../../subjects/jQuery.subject";

export default function runGlobalPage() {
  uspObject.subscribe(() => {
    uspObject.applyStyles();
    uspObject.applyChanges();
  });

  if (window.location.pathname == "/") {
    jQuerySubject.subscribe(() => {
      uspObject.applyStylesSG();
      uspObject.addUsp(".left-off-canvas-menu");
    });
  }

  jQuerySubject.subscribe(() => {
    uspObject.applyStylesSG();
    uspObject.addUspListing();
  });
}
