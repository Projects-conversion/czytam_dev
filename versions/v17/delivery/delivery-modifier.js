import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';
import DeliveryV9Object from '../../v9/delivery/delivery-modifier';
import PaymentV10Object from '../../v10/payment/payment';

class DeliveryModifier extends ModifierSubject {
    constructor() {
        super('.inside-step-shipping', (that) => {});
    }

    applyChanges(){
        this.applyChangesV9();
        this.applyChangesV10();
    }

    applyStyles(){
        require('./delivery.scss');
    }

    applyChangesV9(){
        DeliveryV9Object.applyStyles();
        DeliveryV9Object.applyChanges();
    }

    applyChangesV10(){
        PaymentV10Object.changeHeadlineStep();
        PaymentV10Object.removeStep();
    }
}
const deliveryModifier = new DeliveryModifier();
export default deliveryModifier;