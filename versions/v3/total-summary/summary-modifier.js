import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import progressbarObject from '../progressbar/progressbar-modifier';
import SSLObject from '../ssl/ssl-modifier';

class SummaryModifier extends ModifierSubject {
    constructor() {
        super('#payment_form', (that) => {});
    }

    applyChanges() {
        progressbarObject.addProgressBar(4, '.small-16.large-12.xlarge-13.columns #main', false);
        SSLObject.applyChanges();
    }

    applyStyles() {
        require('./summary.scss');
    }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;