import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { getTextForMobileDelivery } from "../../../utils/delivery-date-getter";
import DeliveryIcon from "../../../assets/sprint1/v1/dostawa.svg";

class KpModifier extends ModifierSubject {
  constructor() {
    super(".productreader", that => {});
  }

  applyChanges() {
    this.addNewDeliveryInfo();
  }

  applyStyles() {
    require("./kp.scss");
  }

  addNewDeliveryInfo() {
    //.productreader > .columns > .row > .columns:not(.col-price)`
    awaitElements(`.wysylka`).then(elem => {
      if (
        ~elem
          .text()
          .indexOf("iedostęp") ||
        ~elem
          .text()
          .indexOf("robocze") ||
        ~elem
          .text()
          .indexOf("rzedsprzeda") ||
        ~elem
          .text()
          .indexOf("odruk")
      ) {
        awaitElements(".jbTotalPrice").then(elem => {
          elem.addClass("jbNoMargin");
          jQuery(".jbDeliveryInforContainer").addClass("jbHide");
        });
      } else {
        elem.after(`
                <div class="jbDeliveryInforContainer">
                    <div class="jbDeliveryInforContainerIcon"><img src="${DeliveryIcon}"/></div>
                    <div class="jbDeliveryInforContainerText"></div>
                </div>
            `);
        elem.addClass("jbHide");
        jQuery(".jbDeliveryInforContainerText").html(
          getTextForMobileDelivery()
        );
        jQuery(".jbWysylka").addClass("jbHide");
      }
    });
  }
}
const kpModifier = new KpModifier();
export default kpModifier;
