import {
    ModifierSubject
} from '../../../modifiers/modifierSubject';
import {
    awaitElements
} from '../../../utils/elements';
import {getTextForDelivery} from '../../../utils/delivery-date-getter';
import DeliveryIcon from '../../../assets/sprint1/v1/dostawa.svg';
import { jQuerySubject } from '../../../subjects/jQuery.subject';

class BasketModifier extends ModifierSubject {
    constructor() {
        super('body', (that) => {});
    }

    applyChanges() {
        this.addNewDeliveryInfo();
    }

    applyStyles() {
        require('./basket-modifier.scss');
    }

    addNewDeliveryInfo() {
        awaitElements('#main > form > table > tbody > tr').then(elems => {
            elems.each((i, obj) => {
                const object = jQuery(obj);
                if((~object.find('.wysylka').text().indexOf('iedostęp')) ||
                    (~object.find('.wysylka').text().indexOf('robocze')) || 
                    (~object.find('.wysylka').text().indexOf('rzedsprzeda'))){
                
                }else{
                    object.find('.wysylka').addClass('jbHide');
                    object.find('.wysylka').after(`
                    <div class="jbDeliveryInforContainer">
                        <div class="jbDeliveryInforContainerIcon"><img src="${DeliveryIcon}"/></div>
                        <div class="jbDeliveryInforContainerText"></div>
                    </div>
                `);
                object.find('.jbDeliveryInforContainerText').html(getTextForDelivery());
                }
            });
        });
    }
}
const basketModifier = new BasketModifier();
export default basketModifier;