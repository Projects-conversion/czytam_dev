import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import progressbarObject from '../progressbar/progressbar-modifier';

import summaryDataTemplate from './summary-data-template.html';
import SSLObject from '../ssl/ssl-modifier';

class SummaryModifier extends ModifierSubject {
    constructor() {
        super('#zgoda', (that) => {});
    }

    applyChanges() {
        progressbarObject.addProgressBar(3, '.small-16.large-12.xlarge-13.columns #main', false);

        this.moveSms();
        this.copySummaryData();
        this.moveContract();
       // this.acceptAgreement();
        this.expandAgreement();
        this.changeElementsIfDeliveryIsOnThePlace();

        SSLObject.applyChanges();
    }

    applyStyles() {
        require('./summary.scss');
    }

    copySummaryData(){
        return Promise.all([
            awaitElements('#main > .alert-box.info-icon:nth-child(1)'),
            awaitElements('#main .small-16.columns'),
            awaitElements('#main .list-label')
        ]).then(([elemTo, headers, data]) => {
            const template = jQuery(summaryDataTemplate);
            
            template.find('.jbRowDataOrderer').prepend(jQuery(headers[0]).html());
            template.find('.jbRowDataOrderer').append(jQuery(data[0]).html())
            template.find('.jbRowAddressOrderer').prepend(jQuery(headers[1]).html())
            template.find('.jbRowAddressOrderer').append(jQuery(data[1]).html())
            template.find('.jbRowAddressDelivery').prepend(jQuery(headers[2]).html())
            template.find('.jbRowAddressDelivery').append(jQuery(data[2]).html())

            elemTo.after(template);
        });
    }

    moveContract(){
        return Promise.all([
            awaitElements('#main > label.long-check'),
            awaitElements('.small-16.small-full.columns')
        ]).then(([elemRef, elemTo]) => {
            const cpyElem = elemRef.clone(true, true).addClass('jbContract');
            elemTo.before(cpyElem);
        });
    }

    moveSms(){
        return awaitElements('#info_zgoda').then(elem =>{
            const elemRef = elem.prev().prev().addClass('jbSms');
            elem.next().after(elemRef);
            //this.jQuery('#uniform-zgoda').parent().after(elemRef);
        });
    }

    changeRegulationsText(){
        
    }

    acceptAgreement(){
        return awaitElements('#uniform-zgoda span input').then(elem => {
            elem.click();
        });
    }

    expandAgreement(){
        return awaitElements('a[target="polityka"]').then(elem => {
            const template = jQuery(`<label><span class="jbAgree">Administratorem danych osobowych jest P.H.U. Tami Wojciech Sterna z siedzibą przy ul. Piotrowskiej 12, 61-353 Poznań, NIP: 7821006266. &nbsp; &nbsp;Rozwiń</span></label>`);

            template.find('.jbAgree').click(() => {
                jQuery('.jbAgree').html(`<span class="jbAgree">Administratorem danych osobowych jest P.H.U. Tami Wojciech Sterna z siedzibą przy ul. Piotrowskiej 12, 61-353 Poznań, NIP: 7821006266. Dane będą przetwarzane w celu świadczenia usług drogą elektroniczną zgodnie z Regulaminem oraz realizacji zamówień złożonych na czytam.pl. Podanie danych jest dobrowolne, jednak konieczne do obsługi i realizacji zamówienia oraz rejestracji zamówienia w serwisie czytam.pl (opcjonalne). Masz prawo dostępu do treści swoich danych, ich poprawienia czy cofnięcia udzielonych zgód na przetwarzanie danych w każdym czasie. Więcej informacji o przetwarzaniu danych osobowych w naszej <a href="/polityka-prywatnosci.html" target="polityka"><u>Polityce Prywatności</u></a></span>`);
            });
            elem.parent().html(template);
        });
    }

    changeElementsIfDeliveryIsOnThePlace(){
        return awaitElements('#main > div.jbRow > .jbRowAddressOrderer > div > strong').then(elem => {
            if(~elem.text().indexOf('własny')){
                jQuery('#main > div:nth-child(13)').addClass('jbHide');
                jQuery('#main > div.jbRow > div.jbRowAddressDelivery').addClass('jbHide2');
            }
        });
    }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;