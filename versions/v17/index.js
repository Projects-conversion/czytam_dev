import { LoadingScreen } from "../../utils/loading-screen";
import { Device } from "../../utils/device";
import runGlobalPage from "./globalPage";

import runv11 from '../v11';

export default function run(){
    let device = new Device();
    device.setAllowed('mobile');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    runv11();
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v17')){
        console.log("dziala wersja v17");
        window.runningVersionSS = 'v17'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v17');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}
