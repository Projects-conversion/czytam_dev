import { ModifierSubject } from "../../../modifiers/modifierSubject";
import { awaitElements } from "../../../utils/elements";
import { getTextForDelivery } from "../../../utils/delivery-date-getter";
import DeliveryIcon from "../../../assets/sprint1/v1/dostawa.svg";
import { jQuerySubject } from "../../../subjects/jQuery.subject";
import { getTextForMobileDelivery } from "../../../utils/delivery-date-getter";
import guard from "../../../utils/guard";
import { ajaxSubject } from "../../../utils/ajax.subject";

let showGreenBox = true;

class BasketModifier extends ModifierSubject {
  constructor() {
    super("body", that => {});
  }

  applyChanges() {
    this.checkIfProductHasDeliveryNotTomorrow();
    this.addFreeDeliveryInfo();
    this.updateProducts();
  }

  applyStyles() {
    require("./basket-modifier.scss");
  }

  checkIfProductHasDeliveryNotTomorrow() {
    awaitElements("form > table > tbody > tr").then(elems => {
      elems.each((i, obj) => {
        const object = jQuery(obj);
        debugger;
        if (
          ~object
            .find(".wysylka")
            .text()
            .indexOf("iedostęp") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("robocze") ||
          ~object
            .find(".wysylka")
            .text()
            .indexOf("rzedsprzeda")
        ) {
          showGreenBox = false;
          return false;
        } else {
          showGreenBox = true;
        }
      });
    });
  }

  updateProducts() {
    ajaxSubject.subscribe(url => {
      if (/kosz_przelicz|kosz_update.php/.test(url)) {
        guard("#main", elem => {
          //if(jQuery('.jbBasket').length == 0){
          this.checkIfProductHasDeliveryNotTomorrow();
          this.addFreeDeliveryInfo();
          //   }
        });
      }
    });
  }

  addFreeDeliveryInfo() {
    awaitElements(`#info_gratis`).then(elem => {
      if (showGreenBox) {
        if (this.jQuery(".jbGreenInfo").length == 0) {
          elem.before(
            `<div class="alert-box jbGreenInfo">
                        <img src="${DeliveryIcon}"/>
                        <span>${getTextForMobileDelivery()}</span></div>`
          );
        }
      }

      if (this.jQuery(".jbIcon").length == 0) {
        elem.after(
          `<div class="alert-box info-icon jbIcon">Do bezpłatnej dostawy brakuje<strong>${elem
            .find("strong")
            .last()
            .text()}</strong> PLN</div>`
        );
      }
    });
  }
}
const basketModifier = new BasketModifier();
export default basketModifier;
