import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import SummaryObjectV9 from '../../v9/summary/summary-modifier';

class SummaryModifier extends ModifierSubject {
    constructor() {
        super('#zgoda', (that) => {});
    }

    applyChanges(){
        this.applyChanesV9();
    }

    applyChanesV9(){
        SummaryObjectV9.applyChanges();
        SummaryObjectV9.applyStyles();
    }

    applyStyles(){
        require('./summary.scss');
    }
}
const summaryModifier = new SummaryModifier();
export default summaryModifier;