import kpModifier from "./kp/kp-modifier";


export default function runGlobalPage() {
  kpModifier.subscribe(() => {
    kpModifier.applyChanges();
  });
}
