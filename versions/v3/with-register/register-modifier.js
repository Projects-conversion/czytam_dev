import { ModifierSubject } from '../../../modifiers/modifierSubject';
import { awaitElements } from '../../../utils/elements';

import progressbarObject from '../progressbar/progressbar-modifier';
import SSLObject from '../ssl/ssl-modifier';

class RegisterModifier extends ModifierSubject {
    constructor() {
        super('form[action="/rejestracja.html"]', (that) => { });
    }

    applyChanges() {
        if(this.jQuery('#info_haslo1').length == 0)
        {
            window.location.replace("https://czytam.pl/koszyk,dostawa.html");
        }
      //  this.redirectToDelivery();
        progressbarObject.addProgressBar(1, '.small-16.columns #main', false);
        this.fillOthersInputs();
        this.showOrHideInputs('#panel-fizyczna #uniform-przenies');
        this.showOrHideInputs('#panel-firma #uniform-przenies');
        this.showOrHideInputs('#panel-biblioteka #uniform-przenies');
        SSLObject.applyChanges();
    }

    showOrHideSecondAddress() {
        return awaitElements('#uniform-przenies').then(elem => {
            elem.find('#przenies').click(() => {
                const selectBox = elem.find('span');

                if (selectBox.hasClass('checked') == true) {
                    jQuery('#main > form .row > div:nth-child(32)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(34)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(36)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(37)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(39)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(40)').addClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(42)').addClass('jbShow');
                } else {
                    jQuery('#main > form .row > div:nth-child(32)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(34)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(36)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(37)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(39)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(40)').removeClass('jbShow');
                    jQuery('#main > form .row > div:nth-child(42)').removeClass('jbShow');
                }
            });
        });
    }

    fillOthersInputs() {
        return awaitElements('.submit-next').then(elem => {
            elem.click(() => {
                if (jQuery('#panel-fizyczna #uniform-przenies span').hasClass('checked') == false) {
                    this.moveData('1');
                }
                else if (jQuery('#panel-firma #uniform-przenies span').hasClass('checked') == false) {
                    this.moveData('2');
                }
                else if (jQuery('#panel-biblioteka #uniform-przenies span').hasClass('checked') == false) {
                    this.moveData('3');
                }
                this.moveData('1');
                this.moveData('2');
                this.moveData('3');
            })
        });
    }

    moveData(value = '') {
        const firstLastName = `${jQuery(`#imie${value}`).val()} ${jQuery(`#nazwisko${value}`).val()}`;
        const street = `${jQuery(`#ulica${value}`).val()}`;
        const homeNo = `${jQuery(`#nr_domu${value}`).val()}`;
        const flatNo = `${jQuery(`#nr_mieszkania${value}`).val()}`;
        const town = `${jQuery(`#miejscowosc${value}`).val()}`;
        const code = `${jQuery(`#kod_pocztowy${value}`).val()}`;
        const country = `${jQuery(`#kraj${value}`).val()}`;

        jQuery(`#d_nazwa${value}`).val(firstLastName);
        jQuery(`#d_ulica${value}`).val(street);
        jQuery(`#d_nr_domu${value}`).val(homeNo);
        jQuery(`#d_nr_mieszkania${value}`).val(flatNo);
        jQuery(`#d_miejscowosc${value}`).val(town);
        jQuery(`#d_kod_pocztowy${value}`).val(code);
        jQuery(`#d_kraj${value}`).val(country);
    }

    showOrHideInputs(selector){
        return awaitElements(selector).then(elem => {
            elem.find('#przenies').click(() => {
                const selectBox = elem.find('span');

                if(selectBox.hasClass('checked') == true){
                    jQuery('#panel-fizyczna > form > hr:nth-child(2)').addClass('jbShow');
                    jQuery('#panel-fizyczna > form > div:nth-child(3)').addClass('jbShow');
                    jQuery('#panel-firma > form > hr:nth-child(4)').addClass('jbShow');
                    jQuery('#panel-firma > form > div:nth-child(5)').addClass('jbShow');
                    jQuery('#panel-biblioteka > form > hr:nth-child(4)').addClass('jbShow');
                    jQuery('#panel-biblioteka > form > div:nth-child(5)').addClass('jbShow');
                }else{
                    jQuery('#panel-fizyczna > form > hr:nth-child(2)').removeClass('jbShow');
                    jQuery('#panel-fizyczna > form > div:nth-child(3)').removeClass('jbShow');
                    jQuery('#panel-firma > form > hr:nth-child(4)').removeClass('jbShow');
                    jQuery('#panel-firma > form > div:nth-child(5)').removeClass('jbShow');
                    jQuery('#panel-biblioteka > form > hr:nth-child(4)').removeClass('jbShow');
                    jQuery('#panel-biblioteka > form > div:nth-child(5)').removeClass('jbShow');
                }
            });
        });
    }

    applyStyles() {
        require('./register.scss');
    }
}
const registerModifier = new RegisterModifier();
export default registerModifier;