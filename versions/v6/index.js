import {LoadingScreen} from "../../utils/loading-screen";
import {Device} from "../../utils/device";
import runGlobalPage from "./globalPage";

export default function run(){
    let device = new Device();
    device.setAllowed('mobile');
    window.runningVersionSSarr = window.runningVersionSSarr || [];
    if (device.isValid() && !~window.runningVersionSSarr.indexOf('v6')){
        console.log("dziala wersja v6");
        window.runningVersionSS = 'v6'; //for page quasii full reloading checking
        window.runningVersionSSarr.push('v6');
        const ls = new LoadingScreen();
        runGlobalPage();
    }
}